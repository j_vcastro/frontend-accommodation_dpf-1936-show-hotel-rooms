# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.15](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.15%0Dv0.2.14) (2021-05-03)


### Features

* **DPF-2024:** update key to get address label (pull request [#37](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/37)) ([febabe4](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/febabe42fe0576e2f13ed8d771ef7ff753f40f2b))
* **DPF-2040:** add skeleton for Accommodation Details component ([33cd192](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/33cd192f6469cfbcf89b3705fb6f6d83f3f93783))


### Bug Fixes

* **DPF-2025:** sync translations to get & instead of html code (pull request [#36](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/36)) ([e613364](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/e613364511d5eda3ad4895863269798664a27dae))

### [0.2.14](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.14%0Dv0.2.13) (2021-04-29)

### [0.2.13](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.13%0Dv0.2.12) (2021-04-28)

### [0.2.12](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.12%0Dv0.2.11) (2021-04-28)

### [0.2.11](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.11%0Dv0.2.10) (2021-04-28)


### Features

* **DPF-2057:** update call to endpoint (pull request [#32](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/32)) ([365e9a0](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/365e9a0007460f601905bd753b1ba5d59741f589))

### [0.2.10](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.10%0Dv0.2.9) (2021-04-21)


### Refactors

* **DPF-2041:** move Star and Type components out of AccommodationCard folder. show Example Components in the docs section. ([b0649b3](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/b0649b3a22ca87731fe941ba84cfd08efe45e21b))
* move the LoadingAccommodationCard into the AccommodationCard folder ([5252d57](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/5252d57a2bf6c47d79f285b64fe94f987c5b5646))
* **DPF-2005:** update Storybook to version 6.2.x ([db1bf4e](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/db1bf4e0f260943493683e1ed4c91b7357b632eb))

### [0.2.9](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.9%0Dv0.2.8) (2021-04-14)

### [0.2.8](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.8%0Dv0.2.7) (2021-04-14)


### Bug Fixes

* **DPF-2022:** add slash when importing components (pull request [#25](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/25)) ([f985d46](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/f985d46c189343a6c15e9d12d5114e1d8375feed))


### Refactors

* **DPF-1970:** remove unused libs (pull request [#23](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/23)) ([0184dda](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/0184dda408b533273dbb36f3bfba855951a686f2))

### [0.2.7](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.7%0Dv0.2.6) (2021-04-08)


### Features

* **DPF-1935:** Show Hotel Type and Rating in Summary Accommodation Card ([43e1b8a](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/43e1b8ad38886b40e05f6352acc496af6b15537e))
* **DPF-1994:** show Accommodation Image in Card (pull request [#20](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/20)) ([3b9f832](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/3b9f83285d8ae6584999086abe07548c546525d9))
* **DPF-1996/7:** add Room and Tag components (pull request [#19](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/19)) ([6b9d355](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/6b9d355d1269b69a66ac5fc95fb5f5be50e393b1))
* **DPF-2012:** update create-component script to allow Components in sub-folders (pull request [#22](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/22)) ([f21f296](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/f21f296b0dac5ae8ee47a3bf9914287e860f5fee))


### Refactors

* **Storybook:** make Docs the default view mode for stories. ([cf58f48](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/cf58f4812ae44b8389e0b8c2cbe12e3107173366))

### [0.2.6](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.6%0Dv0.2.5) (2021-03-31)

### [0.2.5](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.5%0Dv0.2.4) (2021-03-22)


### Bug Fixes

* **DPF-1989:** Fix Accommodation Name Story (pull request [#16](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/16)) ([1f51f49](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/1f51f493fb51a79951b7aa34ac859a2f0d425990))

### [0.2.4](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.4%0Dv0.2.3) (2021-03-19)


### Features

* **dpf-1934:** add accommodation name to summary card (pull request [#14](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/14)) ([801b1fd](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/801b1fd4f5f6f15caac874438374497481a08560))


### Refactors

* **DPF-1987:** Improve SummaryAccommodationCardProviders to receive request data (pull request [#15](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/15)) ([1a22f1d](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/1a22f1d3bc643b3208cbe4c891de7bc8cc0085e0))

### [0.2.3](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.3%0Dv0.2.2) (2021-03-17)


### Features

* **DPF-1976:** Accommodation Card prototype layout (pull request [#13](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/13)) ([8dbb22e](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/8dbb22e81fe9bccdfed2ace4298cc4c87106a326))
* **DPF-1977:** Improve storybook to show themes in toolbar (pull request [#11](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/11)) ([78acc7e](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/78acc7ed562db3363a1b6e423e371db929de87c4))


### Refactors

* **DPF-1980:** Rename Summary Accommodation Card component (pull request [#12](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/12)) ([3c918d5](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/3c918d5d95aa5b935140e4960cb7eeb9a13b960e))

### [0.2.2](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.2%0Dv0.2.1) (2021-03-09)


### Documentation

* **DPF-1973:** Merge strategy docs (pull request [#10](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/10)) ([26789d4](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/26789d474c4029c4092faf911fcb9813f0f073e9))

## [0.2.0](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.2.0%0Dv0.1.0) (2021-03-02)


### Features

* **DPF-1946:** Mock accommodation detail data to implement the accommodation card (pull request [#8](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/8)) ([fca0727](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/fca07276d67f78127bf411058a8cab5c17bd26de))
* **DPF-1958:** add draft HotelCards component to test it in the shell (pull request [#7](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/7)) ([dedb624](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/dedb6247c0cbc9b716124e0c3c45f6acda7a973c))


### Refactors

* **DPF-1950:** Update react-hooks version (pull request [#5](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/5)) ([8651716](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/86517162ee06d4b3725b1592ad7c78d464f6fda5))
* show commit types 'refactor' and 'docs' in the changelog ([85d3f0c](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/85d3f0cffd405acd7c14d916d960687b61fe7bfa))


### Documentation

* **DPF-1954:** add documentation to know how to consume endpoints (pull request [#6](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/6)) ([bd5811e](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/bd5811eb27b2f78a40e273beca3682d503faa553))

## [0.1.0](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.1.0%0Dv0.0.6) (2021-02-16)


### Features

* Initial development release. Storybook deployed at: http://lb.frontend-accommodation.gke-apps.edo.sv/ ([3e0732c](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/3e0732c5a294a7d947adc16cee690b12a212520b))

### [0.0.5](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.0.5%0Dv0.0.4) (2021-02-10)


### Bug Fixes

* **DPF-1898:** update nginx ping url ([3f9dd54](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/3f9dd5402f73974771738d71580528c66771503e))

### [0.0.3](https://bitbucket.org/odigeoteam/frontend-accommodation/compare/v0.0.3%0Dv0.0.2) (2021-02-10)


### Bug Fixes

* **DPF-1948:** test the changelog updates correctly ([2fe08a4](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/2fe08a48b1ee5652eb3a7eaf08d019abdff18959))

### 0.0.2 (2021-02-10)


### Features

* **DPF-1948:** add standard-version utility for automatic changelog generation. includes config for Bitbucket repositories URLs. (pull request [#3](https://bitbucket.org/odigeoteam/frontend-accommodation/pull-requests/3)) ([299e4de](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/299e4dee35a49eb91a604a57693a748bcd32786d))
* **storybook:** add essentials addon and sort stories ([5ab2a2e](https://bitbucket.org/odigeoteam/frontend-accommodation/commits/5ab2a2efd9fbdc042128e2b7e581c82ec8c5bb22))
