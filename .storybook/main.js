module.exports = {
  stories: ["../docs/**/*.stories.@(js|mdx)", "../src/**/*.stories.@(js|mdx)"],
  addons: [
    "@storybook/addon-essentials",
    "@storybook/addon-knobs",
    "storybook-addon-rtl"
  ]
};
