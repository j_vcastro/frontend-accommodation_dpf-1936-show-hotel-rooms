export default function storySortOrder() {
  const storyOrder = [
    "introduction-",
    "getting-started-",
    "developer-guide-",
    "docs-",
    "components-"
  ];

  return (a, b) => {
    const aName = a[0];
    const bName = b[0];

    if (aName.includes("docs-") || bName.includes("docs-")) {
      const aIdx = storyOrder.findIndex(i => aName.indexOf(i) > -1);
      const bIdx = storyOrder.findIndex(i => bName.indexOf(i) > -1);
      return aIdx - bIdx;
    }

    return aName < bName ? -1 : 1;
  };
}
