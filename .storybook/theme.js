import { create } from "@storybook/theming/create";
import ReactAccommodationLogo from "./public/images/React-Accommodation_32_.svg";

export default create({
  base: "light",
  brandTitle: "Frontend Accommodation Storybook",
  brandImage: ReactAccommodationLogo
});
