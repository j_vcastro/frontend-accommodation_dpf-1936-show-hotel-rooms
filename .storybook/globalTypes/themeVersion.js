export default {
  name: "Theme Version",
  description: "Theme version options",
  defaultValue: "COBALT_",
  toolbar: {
    icon: "paintbrush",
    items: [
      { value: "COBALT_", title: "Cobalt" },
      { value: "", title: "Bluestone" }
    ]
  }
};
