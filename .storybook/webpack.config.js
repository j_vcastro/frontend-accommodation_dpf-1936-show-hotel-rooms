const path = require("path");

module.exports = ({ config, mode }) => {
  return Object.assign(config, {
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "../"),
        decorators: path.resolve(__dirname, "./decorators"),
        testUtils: path.resolve(__dirname, "../src/testUtils"),
        public: path.resolve(__dirname, "public/")
      }
    }
  });
};
