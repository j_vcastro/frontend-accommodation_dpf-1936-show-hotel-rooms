import React from "react";
import * as dateFnsLocales from "date-fns/locale";
import { mockWindow } from "@frontend-shell/react-hooks";
import { sites } from "@frontend-shell/react-hooks";
import StorybookModuleProvider from "./StorybookModuleProvider";

const appProvidersDecorator = ({ request, response, abs, loading }) => (
  story,
  { globals }
) => {
  const [brand, locale] = globals.site.split("_");
  const themeVersion = globals.themeVersion;
  const site = sites.getSite(brand, locale);
  const siteLocale = site.locale;
  const themeName = `${themeVersion}${brand}`;

  const content = story();
  const mockOdigeo = {
    trigger: (event, data) =>
      console.log(
        `Integration event sent: name-> ${event}; data->${JSON.stringify(data)}`
      ),
    Session: {
      set: (prop, value) =>
        console.log(
          `Integration session value added: prop-> "${prop}"; value->${JSON.stringify(
            value
          )}`
        )
    }
  };

  return (
    <StorybookModuleProvider
      site={site.websiteCode}
      brand={site.brandCode}
      lang={siteLocale.i18n}
      themeName={themeName}
      dateFnsLocale={dateFnsLocales[siteLocale.dates]}
      currencyLocale={siteLocale.currency}
      response={response}
      window={mockWindow}
      Odigeo={mockOdigeo}
      abs={abs}
      request={request}
      locale={locale}
      loading={loading}
    >
      {content}
    </StorybookModuleProvider>
  );
};

export default appProvidersDecorator;
