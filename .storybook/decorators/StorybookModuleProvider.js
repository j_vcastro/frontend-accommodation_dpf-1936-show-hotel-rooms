import React, { useEffect, useState } from "react";
import { TestingProviders } from "../../src/testUtils";

const defaultLocaleByBrand = {
  ED: "en",
  GO: "fr-FR",
  OP: "it-IT",
  TR: "no-NO"
};

const importTranslations = async (brand, locale) => {
  const translations = await import(`../../translations/${brand}/${locale}.js`);
  return translations.default;
};

const getTranslations = async (brand, locale) => {
  return importTranslations(brand, locale).catch(err => {
    console.warn(err);
    return importTranslations(brand, defaultLocaleByBrand[brand]);
  });
};

const StorybookModuleProvider = ({ brand, locale, children, ...props }) => {
  const [translations, setTranslations] = useState(null);
  useEffect(() => {
    getTranslations(brand, locale).then(setTranslations);
  }, [brand, locale, setTranslations]);
  if (!translations) {
    return null;
  }
  return (
    <TestingProviders
      brand={brand}
      locale={locale}
      translations={translations}
      i18nextOptions={{
        parseMissingKeyHandler: () => "",
        debug: true
      }}
      {...props}
    >
      {children}
    </TestingProviders>
  );
};

export default StorybookModuleProvider;
