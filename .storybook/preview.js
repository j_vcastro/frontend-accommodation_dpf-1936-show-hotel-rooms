import React from "react";
import appProvidersDecorator from "decorators/appProviders.decorator";
import storySortOrder from "./utils/storySortOrder";
import { themeVersion, websites } from "./globalTypes";
import { initializeRTL } from "storybook-addon-rtl";

export const globalTypes = {
  site: websites,
  themeVersion
};
export const parameters = {
  layout: "centered",
  options: {
    storySort: storySortOrder()
  },
  controls: {
    hideNoControlsWarning: true
  },
  backgrounds: {
    default: "ghost",
    values: [
      {
        name: "ghost",
        value: "#eeeeee"
      },
      {
        name: "white",
        value: "#ffffff"
      },
      {
        name: "dark",
        value: "#333333"
      }
    ]
  },
  viewMode: "docs"
};

export const decorators = [appProvidersDecorator({})];

initializeRTL();
