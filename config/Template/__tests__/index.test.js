module.exports = ComponentName => `import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import {
  renderAsync,
  TestingProviders,
} from "@testUtils";
import ${ComponentName} from "../";

const TEST = "test";

const TRANSLATION = {
  filters: TEST
};

afterEach(cleanup);

describe("${ComponentName}", () => {
  test("It should render their children", async () => {
    const content = "text example";
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <${ComponentName} myProp={content} />
      </TestingProviders>
    );

    expect(getByText(content)).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const content = "text example";
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <${ComponentName} myProp={content} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
`;
