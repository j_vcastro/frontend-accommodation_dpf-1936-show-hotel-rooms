module.exports = (StoryTitle, ComponentName) => `import React from "react";
import ${ComponentName} from "./";

export default {
  title: "Components/${StoryTitle}",
  component: ${ComponentName}
};

export const Default = args => {
  return <${ComponentName} {...args} />;
};
Default.args = {
  myProp: "test"
};
`;
