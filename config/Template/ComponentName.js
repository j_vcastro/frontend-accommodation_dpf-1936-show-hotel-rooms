module.exports = ComponentName => `import React from "react";
import PropTypes from "prop-types";
import { Box } from "prisma-design-system";

const ${ComponentName} = ({ myProp }) => {
  return <Box>{myProp}</Box>;
};

${ComponentName}.propTypes = {
  myProp: PropTypes.string.isRequired
};

export default ${ComponentName};
`;
