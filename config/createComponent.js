const fs = require("fs");
const path = require("path");

const storyFileName = "ComponentName.stories.js";
const indexFileName = "index.js";
const componentFileName = "ComponentName.js";
const COMPONENTS_FOLDER = "./src/components";
const TEMPLATES_FOLDER = "./config/Template";
const templateIndex = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  indexFileName
));
const templateComponent = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  componentFileName
));
const templateStory = require(path.join(
  process.cwd(),
  TEMPLATES_FOLDER,
  storyFileName
));
const templateTest = require(path.join(
  process.cwd(),
  `${TEMPLATES_FOLDER}/__tests__/`,
  "index.test.js"
));
const [, , NewFolderName] = process.argv;

/**
 * Create a new component from template
 * @returns
 */
function createComponent(NewFolderName) {
  if (!NewFolderName || NewFolderName === "") {
    console.log("-- Please specify a component/folder name");
    return false;
  }

  try {
    const newFolderPath = path.join(
      process.cwd(),
      `${COMPONENTS_FOLDER}/${NewFolderName}`
    );
    const componentName = newFolderPath.split(path.sep).pop();
    const storyTitle = NewFolderName.match(/($[a-z])|[A-Z][^A-Z]+/g)
      .join(" ")
      .replace(/\/ /g, "/");

    if (!fs.existsSync(newFolderPath)) {
      fs.mkdirSync(newFolderPath, { recursive: true });
      fs.mkdirSync(path.join(newFolderPath, "__tests__"));

      fs.writeFileSync(
        `${newFolderPath}/${componentName}.js`,
        templateComponent(componentName)
      );
      fs.writeFileSync(
        `${newFolderPath}/${componentName}.stories.js`,
        templateStory(storyTitle, componentName)
      );
      fs.writeFileSync(
        `${newFolderPath}/index.js`,
        templateIndex(componentName)
      );
      fs.writeFileSync(
        `${newFolderPath}/__tests__/index.test.js`,
        templateTest(componentName)
      );
      console.info(`-- Component Created at ${newFolderPath}`);
      return true;
    } else {
      console.warn(
        "-- Component with this name ALREADY exists. Specify another name!"
      );
    }
  } catch (e) {
    console.error(e);
  }
}

createComponent(NewFolderName);
