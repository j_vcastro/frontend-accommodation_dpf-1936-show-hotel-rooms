const { promises } = require('fs');

return promises
  .mkdir('.out/engineering', { recursive: true })
  .then(() => promises.writeFile('.out/engineering/ping.html', 'pong'))
  .catch(err => {
    if (err) throw err;
  });
