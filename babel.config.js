module.exports = api => {
  api.cache.never();
  const { ESM_BUILD, NODE_ENV } = process.env;
  let modulesConfig = {};
  if (NODE_ENV === "test") {
    modulesConfig = {};
  } else if (NODE_ENV === undefined && ESM_BUILD === "true") {
    modulesConfig = { modules: false };
  }

  const presets = [
    [
      "@babel/preset-env",
      {
        targets: "> 0.25%, not dead",
        useBuiltIns: "usage",
        corejs: 3,
        ...modulesConfig
      }
    ],
    "@babel/preset-react"
  ];
  const plugins = [
    [
      "module-resolver",
      {
        root: ["./"],
        alias: {
          "^@components(.*)": "./src/components\\1",
          "^@testUtils(.*)": "./src/testUtils\\1"
        }
      }
    ]
  ];
  return { presets, plugins };
};
