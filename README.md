# @FRONTEND-ACCOMMODATION #

**@Frontend-accommodation is a components lib, to be used inside @frontend-shell/core project.**

### Technological stack ###
This module is build with the following technologies:

* [React & React-dom](https://reactjs.org/)
* [Apollo-client](https://www.apollographql.com/docs/)

We are implementing some others techs in development to help us make our code more efficient, reusable, maintenable and scalable:

* Jest
* Testing-library
* Babel
* Prettier
* eslint

Storybook is used for prototyping and developing components:

* [Storybook](https://storybook.js.org/)

We use the Prisma Design System for our common design components:

* [Prisma Design System](https://bitbucket.org/odigeoteam/prisma-design-system/)

### How do I get set up? ###

Firstly:

 * execute ```npm ci``` in the root folder
 
     This will install all the necessary node modules. Note: you use ```npm ci``` and not ```npm install```. You should only run the install command when you are adding or updating a module.  
     
Secondly, build the project and deploy the Storybook:

 * execute ```npm run start``` in the root folder
 
    After this command has run, the terminal will display a link that you can open to view a local version of the storybook.


#### Development commands:

* ```npm run create-component MyNewComponent``` or ```npm run create-component Subfolder1/Subfolder2/MyNewComponent```:

    Create a new component scaffolding. The component can be specified inside sub-folders.

* ```npm run test```:

    Runs js test.

* ```npm run lint```:

    Check the code format.

* ```npm run pre-commit```:

    Formats your code following the code style guide and runs js test.

#### Production commands:

  * ```npm run prepublishOnly```:

      Build transpiled code and sourcemaps in dist/ folder, discarding tests.
        
### Frontend Shell ###

**Important:** This module has to be used as dependency in @frontend-shell/core module, even for development. You will be able to run test independly, without importing it in @frontend-shell/core, but for dev mode, you will need to have @frontend-shell/core in your local (follow the @frontend-shell/core intructions to know how to start developing frontend modules: https://bitbucket.org/odigeoteam/frontend-shell/src/master/README.md).

### Who do I talk to? ###

* Owner: [Dreampack](https://jira.odigeo.com/wiki/display/KB/DP+Fusion+Program)
