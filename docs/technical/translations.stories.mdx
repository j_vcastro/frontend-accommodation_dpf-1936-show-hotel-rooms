import { Meta, Canvas, Preview, Story } from '@storybook/addon-docs/blocks';

<Meta title='Docs/Developer Guide/Translations' />

# Translations

#### Table of Contents
1. [.translationsrc.json](#translationssrc.json)
2. [Generating New Translations](#generating-translations)
3. [Using Translations in Code](#using-translations-in-code)
4. [Testing Translations in the Storybook](#testing-translations-in-storybook)

<hr/>

In order to use the content present in the CMS we need to specify which keys we want to use. The tool we use to fetch the content was developed by the OF2 team, the current version **only fetches mobile keys**, so we can’t use desktop keys by now. [^1]

[^1]: We have a ticket to analyse using desktop keys, https://jira.odigeo.com/browse/DPF-1938

<br/>

## TranslationsSrc.json

The keys needed must be included in the _.translationsrc.json_ file,  present in the src folder of the project. For example:

<figure style={{ maxWidth: '500px' }}>
<img src="https://drive.google.com/uc?export=view&id=1LroamddRJiz5hPU7S13n-sY3898OQnRO" alt="Google Drive image"/>
<figcaption>example translations src</figcaption>
</figure>

It has the following structure:

```json
{
  "dest": "translations",
  "scopes": { },
  "transformations": [
      {
        "inputPath": [],
        "outputPath": [],
        "scope": [], // optional
        "args": [], // optional
        "silence": {} //optional
      }
  ]
}
```

Now let's explore each of these properties in more detail:

### dest

The _dest_ property defines into which folder the results will be stored. The results are a set of JSONs —one for each brand market— containing the mapping key-content for that market:

<figure style={{ maxWidth: '500px' }}>
<img src="https://drive.google.com/uc?export=view&id=1TrxRNEyt1Df4G77t5u1sP940E9KW0fE2" alt="Google Drive image"/>
</figure>

### scopes

The _scopes_ property defines the possible keys market scopes, this allows us to easily group multiple markets into a single scope which we can use inside each transformation. For example:

```json
{
  "scopes": { 
    "ARABIC_SITES": ["OP_ar-AE", "ED_ar-SA"]
  }
}
```
In the example above, we have created a group called ARABIC_SITES that we can re-use; rather than having to write ["OP_ar-AE", "ED_ar-SA"] everytime.

See the [scope property](#scope) section for more information on what these scopes actually do.

### transformations

The _transformations_ property defines the translations we request: 

It is made up of two mandatory elements: _inputPath_ and _outputPath_  and three optional properties of: _scope_, _args_ and _silence_.

#### inputPath

The _inputPath_ refers to the **category.key** of the content in the CMS where the content is going to be found.
inputPath expects an array of two values:

  * the first is the _category_
  * the second is the _key_

So, if we want to retrieve for example, the key "accommodation.noresults.title" from the category "dynpack" we would set an inputPath of:
`["dynpack", "accommodation.noresults.title"]`

Remember that the translations system **only looks for mobile keys**. See Footnote [^1]
      
#### outputPath

The _outputPath_ is the path that we will use to access the content in our project. The outputPath **can be diferent** from the inputPath. For example:

```json
{
  "transformations": [
    {
      "inputPath": ["html", "header.description"],
      "outputPath": ["test", "translations", "text"]
    }
  ]
}
```

In this example, we are retrieving a key from "html.header.description". 
But we are outputting it to our application with a value of "test.translations.text".
We will see later how we use these outputted values in the [Using Translations in Code](#using-translations-in-code) section.

#### scope      

The _scope_ property denotes the markets for which the key will be translated. 
By default, the translation system will attempt to retrieve key values from all markets. 
However there are some keys which only exist in certain markets. Therefore, we use _scope_ to tell the system to only retrieve a key in a particular market/s.
For example:

```json
{
  "inputPath": ["residentDiscount", "results.discountIncluded"],
  "outputPath": ["results", "resident", "itinerary", "message"],
  "scope": ["ED_es-ES"]
}
```
In the above example; we are telling the system this key only exists in eDreams Spain.
If we did not add a scope, then the translations system would throw an error for all markets where this key does not exist.

This is also where we can use our _scopes_ property that we defined earlier. 
For example, if we have the following scopes defined:

```json
"scopes": {
  "PRIME": [
      "ED_en",
      "ED_en-GB",
      "ED_en-US",
      "ED_es-US",
      "ED_es-ES",
      "ED_fr-FR",
      "ED_it-IT",
      "ED_pt-PT",
      "GO_fr-FR",
      "OP_en",
      "OP_de-DE",
      "OP_fr-FR",
      "OP_en-GB"
    ],
    "TRAVELLINK": [
        "TR_da-DK",
        "TR_de-DE",
        "TR_en",
        "TR_fi-FI",
        "TR_is-IS",
        "TR_no-NO",
        "TR_sv-SE"
     ]
},
```

Then we can use one or more of these scopes arrays to set our scope property. For example:

```json
{
  "inputPath": ["variables", "brand.name.prime"],
  "outputPath": ["variables", "brand", "name", "prime"],
  "scope": ["PRIME"]
}
```

In the above example we are telling the system to retrieve this key for all markets listed in the PRIME scopes array.

##### scope filtering

We can also add some extra options to a scope with the symbols: “!”, “?”  

  * The ! symbol means “except”.  
  * The ? symbol means “optional”.  
  
This allows us to do more advanced filtering, for example:

```json
{
  "inputPath": ["apptouchpoint", "results2.text2"],
  "outputPath": [ "apptouchpoint", "results", "middle", "text"],
  "scope": ["!TRAVELLINK"]
}
```

In this example we are telling the system to retrieve the key for all markets _except_ those listed in the TRAVELLINK scopes array.
      
#### args

The _args_ property indicates the name that we will give to each of the arguments that the content receives, in order (note that in the CMS the keys are identified by the order $1, $2, .. and not by an id.) For example:

```json
{
  "transformations": [
    {
      "inputPath": ["filtersSummary", "matchingAmount"],
      "outputPath": ["filters", "matchingAmount"],
      "args": ["min", "max"]
    }
  ]
}
```

In the above example, we are saying that the translation content receives two arguments that we will call "min" and "max" when calling the translation function.

#### silence

The _silence_ property sets some additional options; noMatchArgs indicates for which markets skip the attributes matching, it means that the content for those markets won't expect for an argument. For example:

```json
{
  "inputPath": ["results", "legal_text.default_hidden_pbd"],
  "outputPath": ["results", "itinerary", "legalDisclaimer", "defaultPrice"],
  "args": ["method"],
  "silence": {
    "noMatchArgs": [
      "OP_en-AU"
    ]
  }
}
```

In the above example, we are telling the system that the Opodo Australia site does _not_ expect an argument for this key.

<br/>
<br/>

## Generating Translations

Whenever you add a new translation to .translationsrc.json; you need to generate the updated translations files in order to use it in the application. 
To do this simply run the following command:

```
npm run translate
```

After running this command we can see the json files to know which are the changes and we can check these are the changes we want.
Notice that the translation will be executed automatically when building the project.

*NOTE: you need to be connected to the VPN or inside the VDI for the translate command to work.*

<br/>
<br/>

## Using Translations in Code

When we are writing code and need to use a translation, we use the translation function.

The translation function receives the _outputPath_ of a key defined in the _translationsrc.json_ file and renders the content corresponding to the current brand-market.

It is imported from the useTranslation provider. See the following example:

```js
import React from "react";
import { useTranslation } from "@frontend-shell/react-hooks";

const TranslationsTest = () => {
  const { t } = useTranslation();
  return (
      <>{t("test.translations.text")}</>
  );
};

export default TranslationsTest;
```

### How to add translations in JS Tests?
   
To use the translations for the tests we have to set the translations property, as follows:

```js
const TRANSLATION = {
  test: {
    translations: {
      text: TRANSLATIONS_TEXT
    }
  }
};

<TestingProviders translations={TRANSLATION}>
  ...
</TestingProviders>
```
This way, we specify the key (output path) in an object manner, and set the text that we want to assign to it for the testing.

<br/>
<br/>

## Testing Translations in Storybook

You can easily test your translations in the Frontend Accommodation Storybook by using the **globe icon** in the toolbar to change the website to another language. 

<figure style={{ maxWidth: '500px' }}>
<img src="https://drive.google.com/uc?export=view&id=1QqOhMGi5D8PiLTHYLXluPt2Xd8VTjhBm" alt="Google Drive image"/>
<figcaption>Select a website from the globe in the Toolbar</figcaption>
</figure>

Stories automatically re-render when you select a new website, showing the correct language and branding for that website. 
The following Story provides a simple translation test: Try it out now by selecting a different website and seeing the language change.

<Canvas>
<Story id="docs-example-components-translations--default" />
</Canvas>

<br/>

[Back To Top](#translations)

<br/>
