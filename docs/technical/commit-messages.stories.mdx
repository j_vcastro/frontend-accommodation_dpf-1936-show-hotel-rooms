import { Meta } from '@storybook/addon-docs/blocks';

<Meta title='Docs/Developer Guide/Commit Messages' />

# Commit Messages

#### Table of Contents
1. [Quick Summary](#quick-summary)
2. [Commit Types](#commit-types)
3. [Commit Title and Body](#commit-title-and-commit-body)
4. [Squash Merge Strategy](#squash-merge-strategy)

<hr/>

In the Frontend Accommodation repository we are using the **conventional commits**
format for our commit messages. This is different from OneFront. The API standard is:

```git
<type>[optional scope]: <description>

[optional body]

[optional footer]
```

For a full rundown of conventional commits, see: [the official conventionalcommits.org documentation](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)

## Quick Summary

In Frontend-accommodation all commits messages *in upstream* must follow the format:

```type(JIRA-ID): your commit message (pull request #link)```

```type``` is mandatory. It should be one of:

* fix
* feat
* refactor
* docs
* chore

```JIRA-ID``` is mandatory. It should be your ticket number.

```your commit message``` is mandatory. Explains what the commit does.

```pull request #link``` if the commit is PR squash merge then you **must** add the pull request link to the end of your commit message.

Remember that you can also add an optional body to your commit message to provide more information.

## Commit Types

In our repository we have five commit types:

* fix
* feat
* refactor
* docs
* chore

Remember that **chores do not apppear in the changelog**.

Let's review each of these in detail:

### fix
a commit of the type fix patches a bug in your codebase

if you are working on JIRA Ticket Type of Bug then you should use fix.

### feat
a commit of the type feat introduces a new feature to the codebase

if you are working on JIRA Ticket Type of Story then you should use feat.

### refactor
a commit of the type refactor improves a current implementation without adding a new feature or fixing a bug.

if you are working on JIRA Ticket Type of Maintenance then you may use refactor.

### docs
a commit of the type docs relates to changes in documentation of the codebase

### chore
a commit of the type chore typically relates to increase version numbers, or other utility changes with no production code change.

Chore is the commit type Jenkins uses to make a new release of the codebase.


## scope

We use the conventional-commit concept of *scope* to reference to the JIRA ticket the commit is for.

The format is as follows,

``` type(scope): your message ```

For example:

``` feat(DPF-123): add new translations group for Arabic sites ```

This has the advantage of clearly listing every JIRA ticket in the generated changelog.
The example above would be rendered as the following in the changelog:

> Features
>
> **DPF-123**: add new translations group for Arabic sites

<br/>

## Commit Title and Commit Body

All of this is just standard git practice. Find more information on the internet in lots of places. A good resource: https://chris.beams.io/posts/git-commit/

Your commit title -- the first line of the commit message -- must follow the agreed format, for example:

``` docs(DPF-123): documenting commit messages ```

But remember that you can always add a "body" to your commit to provide a more detailed description. For example:

```git
docs(DPF-123): documenting commit messages

Provide some developer guides for the conventional commits
syntax, and also some helpful links for common git practictes,
and merge strategies.
```

From the gitcommit manual:

> Though not required, it’s a good idea to begin the commit message with a single short (less than 50 character)
> line summarizing the change, followed by a blank line and then a more thorough description.
> The text up to the first blank line in a commit message is treated as the commit title,
> and that title is used throughout Git.


<br/>
<br/>

## Squash Merge Strategy

Interestingly, Conventional-commits are designed to be used on *merge commit message* only, not on every single commit.
This is from the conventional-commits website:

> As an open-source maintainer, squash feature branches onto master
> and write a standardized commit message while doing so.

If we want to imitate this strategy in our repository; then we should try and follow the principle that:

* #### 1 JIRA Ticket = 1 Fork = 1 Pull Request = 1 Commit in Upstream

To do this you need to use the squash merge strategy for your PR. Understand what a squash merge is before you do this!
Some useful resources are: [Squashing your Pull Requests](https://cloudfour.com/thinks/squashing-your-pull-requests/#squash-and-merge)

If you squash, your **PR merge commit message** is **extremely important**. Because it will be the single record of your work in upstream.

You **must manually change** the message from the bitbucket default. It must be of the format:

```type```(```JIRA-ID```): *your commit message* (```pull request #link```)

Example:

```feat(DPF-1234): commit a feature with a single commit to upstream (pull request #123)```

### Squashing in Bitbucket

*Note: the process explained below works the same for branches and forks.*

To perform a squash merge from a pull request in Bitbucket, create the Pull Request as you normally would.

We recommended that your **Pull Request Title** follows the conventional-commit syntax.
For example, if you are creating a new feature with a JIRA ticket of DP-123 then your Pull Request Title would be:
*"feat(DP-123): a message describing the change"*

The review process of the Pull Request remains the same. People can comment on code, you can make additional commits based on feedback, people can approve or deny the PR.
Once you PR meets the requirements of the code review process. <mark>Link to code review agreements once we have these</mark>
It is ready to be merged.

Select the Merge button from the PR screen:

<figure style={{ maxWidth: '700px' }}>
<img src="https://drive.google.com/uc?export=view&id=1S9AL7p1qYxCiQbrBIB0nqDUfl2WPtGc4" alt="Bitbucket Merge PR Window"/>
<figcaption>Bitbucket Merge pull request popup</figcaption>
</figure>

You will be presented with the normal Bitbucket "Merge pull request" popup.
If you look at the bottom of the popup you will see a dropdown called **merge strategy**. From this dropdown you need to select **Squash**.

<figure style={{ maxWidth: '700px' }}>
<img src="https://drive.google.com/uc?export=view&id=1--DfEfoBUrshZkIiLiP93_bg9ecgHMLP" alt="Bitbucket Merge Strategy"/>
<figcaption>Changing the merge strategy to Squash</figcaption>
</figure>

After you have selected squash the default merge Commit message will change. It will be something like "Merged in...".
We must **change this message** to make it conventional-commit compatible.

If you remember from the beginning of this document; our required commit message format is:

```type```(```JIRA-ID```): ```your commit message``` (```pull request #link```)

Notice that the end part of this format -- the pull request link -- has been auto-generated by Bitbucket in the default message.
We can use this when writing our conventional-commit message:

<figure style={{ maxWidth: '700px' }}>
<img src="https://drive.google.com/uc?export=view&id=1AcFN-TFyR_WFhdeovX_RYgPgJV3trrwl" alt="Bitbucket Pull Request Link"/>
<figcaption>Do not delete the generated pull request link</figcaption>
</figure>

Keeping the pull request link intact, we can modify the rest of the message to make a conventional-commit.

The first line of the commit message must follow the convention. But remember you can always add more information to the commit
by adding a blank line below the commit title and then adding a more detailed description:

<figure style={{ maxWidth: '700px' }}>
<img src="https://drive.google.com/uc?export=view&id=1_sBx-nFshjuRxmTduiWZ4dxvEMa5YJGj" alt="Bitbucket convetional merge message"/>
<figcaption>Change your merge message to be conventional-commit friendly</figcaption>
</figure>

You are now able to squash merge!

Press the Merge button and if everything is setup correctly the Pull Request will be merged
and you will see a *single* commit in upstream, with the message you set in the Merge popup. This commit contains all of the changes
from your Pull Request.

<figure style={{ maxWidth: '700px' }}>
<img src="https://drive.google.com/uc?export=view&id=1E4gcetA_0m-bC-2-52lihSANiGipZ1A5" alt="Bitbucket commit history squash"/>
<figcaption>
A squash merge results in a single commit in upstream containing all changes</figcaption>
</figure>
<br/>

Note: a squash commit is not marked as a merge in the commit history
(This is because technically; a squash is *not a real merge*; what it actually does is takes all of your changes, copies them, and adds them to upstream as a single *new* commit)





<br/>
<br/>

[Back To Top](#commit-messages)

<br/>


