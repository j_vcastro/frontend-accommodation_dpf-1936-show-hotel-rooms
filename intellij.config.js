// eslint-disable-next-line no-undef
System.config({
    "paths": {
        'decorators/*': './.storybook/decorators/*',
        '@components/*': './src/components/*',
        '@testUtils/*': './src/testUtils/*',
    },
});
