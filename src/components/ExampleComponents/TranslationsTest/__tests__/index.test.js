import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync as render, TestingProviders } from "@testUtils";
import TranslationsTest from "../";

const TRANSLATION = {
  accommodationCard: {
    freeCancelationLabel: "FREE hotel cancellation"
  }
};

afterEach(cleanup);

describe("TranslationsTest", () => {
  test("It should render the translated text", async () => {
    const { getByText } = await render(
      <TestingProviders translations={TRANSLATION}>
        <TranslationsTest />
      </TestingProviders>
    );

    const textElement = getByText(
      TRANSLATION.accommodationCard.freeCancelationLabel
    );
    expect(textElement).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await render(
      <TestingProviders translations={TRANSLATION}>
        <TranslationsTest />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
