import React from "react";
import { BannerBox, Box, Text } from "prisma-design-system";
import { useTranslation } from "@frontend-shell/react-hooks";

/**
 * Component for testing translations.
 * Uses the useTranslation provider to return a translated value from the sample CMS key.
 */
const TranslationsTest = () => {
  const { t } = useTranslation();
  return (
    <BannerBox type="semanticValueToBeDefined_5">
      <Box p={4}>
        <Text>{t("accommodationCard.freeCancelationLabel")}</Text>
      </Box>
    </BannerBox>
  );
};

export default TranslationsTest;
