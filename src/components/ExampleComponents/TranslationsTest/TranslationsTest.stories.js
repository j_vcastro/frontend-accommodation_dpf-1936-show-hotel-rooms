import React from "react";

import TranslationsTest from "./";

export default {
  title: "Docs/Example Components/Translations",
  component: TranslationsTest,
  parameters: {
    componentSubtitle: "Dummy Component"
  }
};

export const Default = () => {
  return (
    <div style={{ maxWidth: "35em" }}>
      <em>
        Tests the storybook Translations mechanism. Use the{" "}
        <strong>globe icon</strong> in the toolbar to change the language.
        Sample CMS key used is "dynpack.roomSelection.free.cancelation.text":
      </em>
      <br />
      <hr />
      <div>
        <TranslationsTest />
      </div>
    </div>
  );
};
