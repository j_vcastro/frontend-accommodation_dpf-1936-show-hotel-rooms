import React from "react";

import TestComponent from "./";

export default {
  title: "Docs/Example Components/Test Component",
  component: TestComponent,
  argTypes: { click: { action: "Button click" } },
  parameters: {
    componentSubtitle: "Dummy Component"
  }
};

export const Default = args => {
  return <TestComponent {...args} />;
};
Default.args = {
  content: "Test"
};
