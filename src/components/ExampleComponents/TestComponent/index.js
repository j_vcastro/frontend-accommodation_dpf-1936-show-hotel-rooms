import React from "react";
import { Button } from "prisma-design-system";

/** Just a test component used to demonstrate the Storybook. */
const TestComponent = ({ content, click }) => {
  return (
    <div>
      <Button onClick={click} size="medium" type="default">
        {content}
      </Button>
    </div>
  );
};

export default TestComponent;
