import React, { useState } from "react";
import PropTypes from "prop-types";
import { Flex, Card } from "prisma-design-system";
import ImageSlideShow from "@components/ImageSlideShow";
import AccommodationImage from "@components/AccommodationCard/AccommodationImage";

/** Prototype accommodation card, demonstrates a responsive layout: one column in small screens, two columns in medium, and then three columns in large */
const AccommodationCardPrototype = ({
  cardMinHeight,
  infoMinWidth,
  infoMinHeight,
  priceMinWidth,
  priceMinHeight,
  imageMinHeight,
  imageUrL,
  firstColFlexBasis,
  secondColFlexBasis,
  accommodationImages,
  showSlideShow,
  ...rest
}) => {
  const [showControls, setShowControls] = useState(false);
  if (!showSlideShow) accommodationImages = [];

  return (
    <>
      {/* the hidden prop on the Card is a prisma fix to prevent rounded borders being clipped */}
      <Card
        p={0}
        onMouseEnter={() => setShowControls(true)}
        onMouseLeave={() => setShowControls(false)}
        onFocus={() => setShowControls(true)}
      >
        <Flex
          flexDirection="row"
          flexWrap="wrap"
          minHeight={`${cardMinHeight}`}
        >
          <Flex
            flex={`${firstColFlexBasis}`}
            alignItems={"stretch"}
            style={{ background: "red" }}
            minHeight={`${imageMinHeight}`}
          >
            {accommodationImages?.length > 0 ? (
              <ImageSlideShow
                showControls={showControls}
                images={accommodationImages}
              />
            ) : (
              <AccommodationImage imageUrl={imageUrL} altText="" />
            )}
          </Flex>
          <Flex flex={`${secondColFlexBasis}`}>
            <Flex flexGrow={1} flexDirection="row" flexWrap="wrap">
              <Flex
                flex={`3 1 ${infoMinWidth}`}
                alignItems={"stretch"}
                minHeight={`${infoMinHeight}`}
                p={4}
                style={{ background: "orange" }}
              >
                <div>the info</div>
              </Flex>
              <Flex
                flex={`1 1 ${priceMinWidth}`}
                alignItems={"stretch"}
                minHeight={`${priceMinHeight}`}
                p={4}
                style={{ background: "yellow" }}
              >
                <div>The price</div>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
      </Card>
    </>
  );
};

AccommodationCardPrototype.defaultProps = {
  firstColFlexBasis: "1 1 150px",
  secondColFlexBasis: "5 1 500px",
  cardMinHeight: "250px",
  infoMinWidth: "250px",
  priceMinWidth: "250px",
  infoMinHeight: "150px",
  priceMinHeight: "150px",
  imageMinHeight: "150px",
  showSlideShow: true,
  imageUrL:
    "https://images.squarespace-cdn.com/content/v1/54332d95e4b0a62f5011f96e/1596913086344-PWJ3S74IU5RFTCDRQBD4/ke17ZwdGBToddI8pDm48kB6N0s8PWtX2k_eW8krg04V7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1URWK2DJDpV27WG7FD5VZsfFVodF6E_6KI51EW1dNf095hdyjf10zfCEVHp52s13p8g/PB291074+copy.JPG?format=2500w",
  accommodationImages: [
    {
      url:
        "https://www.edreams.com/pictures/zo1vttxy0ty1/1000000/600000/597700/597669/585bbe09_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/723kuvwz7651/1000000/600000/597700/597669/585bbe09_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/0284yxtury8l/1000000/600000/597700/597669/5749b1c2_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/452xtuyvf495/1000000/600000/597700/597669/5749b1c2_t.jpg",
      type: "ROOM",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/036ntzyvgqr8/1000000/600000/597700/597669/93f5003d_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/xd1lttxx61h7/1000000/600000/597700/597669/93f5003d_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/03ivwzxy45sd/1000000/600000/597700/597669/ac0309dc_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/25cnwtyy2qr3/1000000/600000/597700/597669/ac0309dc_t.jpg",
      type: "ROOM",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/08agxtux4xxk/1000000/600000/597700/597669/bb1b99d0_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/kz68wzvw9z9s/1000000/600000/597700/597669/bb1b99d0_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/0d08wxtz5o26/1000000/600000/597700/597669/6f5c3422_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/43z0vzuunnaf/1000000/600000/597700/597669/6f5c3422_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/0n7iwxxzfnsf/1000000/600000/597700/597669/8aa4538a_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/dsfduxty0036/1000000/600000/597700/597669/8aa4538a_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/0x9ezyyzr0u7/1000000/600000/597700/597669/ffe3843f_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/p774uzwv8yws/1000000/600000/597700/597669/ffe3843f_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/123pyxtwdomj/1000000/600000/597700/597669/30df0212_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/f41styzyfu10/1000000/600000/597700/597669/30df0212_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/13ayuwvzz12c/1000000/600000/597700/597669/d8ddb273_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/2d4cuxyyv35d/1000000/600000/597700/597669/d8ddb273_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/13f8zyuwx36k/1000000/600000/597700/597669/c687dc40_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/hvu9wxtye3sd/1000000/600000/597700/597669/c687dc40_t.jpg",
      type: "GENERAL",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/156zvvwufwkp/1000000/600000/597700/597669/6e7dd591_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/gk2qwwwt4wt4/1000000/600000/597700/597669/6e7dd591_t.jpg",
      type: "GENERAL",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/15rgzwyypmmh/1000000/600000/597700/597669/5a1a5051_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/554kwzvuz9o6/1000000/600000/597700/597669/5a1a5051_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/1734uzuu29yq/1000000/600000/597700/597669/0993172d_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/z769ztxzmfb0/1000000/600000/597700/597669/0993172d_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/18gstzxvjbi7/1000000/600000/597700/597669/3da5d45c_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/881gwtxwq2a1/1000000/600000/597700/597669/3da5d45c_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/1c49wywxbk04/1000000/600000/597700/597669/02e9b5e3_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/unuaxvtyn2gx/1000000/600000/597700/597669/02e9b5e3_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/1j94yyvz5c4j/1000000/600000/597700/597669/8b29e3f3_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/bin1yzyuv6u9/1000000/600000/597700/597669/8b29e3f3_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/1l05yuwv37dz/1000000/600000/597700/597669/78d8ba1e_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/6p0atwyx3821/1000000/600000/597700/597669/78d8ba1e_t.jpg",
      type: "GENERAL",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/2172wxuzn68h/1000000/600000/597700/597669/19c4a622_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/ck27vtvz8l3g/1000000/600000/597700/597669/19c4a622_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/21c0yztw695g/1000000/600000/597700/597669/bae93070_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/ap9jxtzyuu41/1000000/600000/597700/597669/bae93070_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/240fytwzs60h/1000000/600000/597700/597669/f8a336d5_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/n1s5tuvv32t0/1000000/600000/597700/597669/f8a336d5_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/2533xxwvljn2/1000000/600000/597700/597669/63404b82_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/n1r3zuvy9nwm/1000000/600000/597700/597669/63404b82_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/26m5vtvu07o3/1000000/600000/597700/597669/7416cfad_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/b983vyttvy26/1000000/600000/597700/597669/7416cfad_t.jpg",
      type: "ROOM",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/27vlwxxyq24k/1000000/600000/597700/597669/d33f0afe_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/s165xxuvdg87/1000000/600000/597700/597669/d33f0afe_t.jpg",
      type: "HOTEL",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/28y7wvwti5qn/1000000/600000/597700/597669/ccf3b459_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/yxr1vyuymx5l/1000000/600000/597700/597669/ccf3b459_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/2e9ftwwuru0e/1000000/600000/597700/597669/b3216c7b_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/1g46uuwy8cgb/1000000/600000/597700/597669/b3216c7b_t.jpg",
      type: "HOTEL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/2h83zzwy1645/1000000/600000/597700/597669/fe86cf67_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/vu4wxwwyq7wq/1000000/600000/597700/597669/fe86cf67_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/2p0buwyzm0i4/1000000/600000/597700/597669/0510df22_z.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/ualnxtvv4vcz/1000000/600000/597700/597669/0510df22_t.jpg",
      type: "GENERAL",
      quality: "MEDIUM"
    },
    {
      url:
        "https://www.edreams.com/pictures/2u54xxyxi950/1000000/600000/597700/597669/c98fe09b_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/0n9sxzzxiqm4/1000000/600000/597700/597669/c98fe09b_t.jpg",
      type: "GENERAL",
      quality: "LOW"
    },
    {
      url:
        "https://www.edreams.com/pictures/2v3rztuv32hn/1000000/600000/597700/597669/4ac8a5de_b.jpg",
      thumbnailUrl:
        "https://www.edreams.com/pictures/tzxftyyu676e/1000000/600000/597700/597669/4ac8a5de_t.jpg",
      type: "ROOM",
      quality: "LOW"
    }
  ]
};

AccommodationCardPrototype.propTypes = {
  /** flex basis for the image section */
  firstColFlexBasis: PropTypes.string,
  /** flex basis for the info and price section */
  secondColFlexBasis: PropTypes.string,
  infoMinHeight: PropTypes.string,
  infoMinWidth: PropTypes.string,
  priceMinWidth: PropTypes.string,
  imageMinHeight: PropTypes.string
};

export { AccommodationCardPrototype };
