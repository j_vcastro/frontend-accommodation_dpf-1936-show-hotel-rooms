import React from "react";
import { Flex, Box } from "prisma-design-system";

/** A Template Prototype Component: make a copy and modify me.
 *
 * Prototypes are for investigation and ideation. Use them test how layouts
 * work on different screens. Use them to work with UX on design proposals.
 * Use them to split tickets for epics. Use them to help documentation and training.
 * Whatever you like: just Don't use them in production.
 */
const Prototype = ({ ...rest }) => {
  return (
    <>
      {/* Begin your prototype here */}
      <Flex alignItems={"center"} justifyContent={"center"}>
        <Box>
          <>Hello Prototype</>
        </Box>
      </Flex>
    </>
  );
};

Prototype.propTypes = {};

export { Prototype };
