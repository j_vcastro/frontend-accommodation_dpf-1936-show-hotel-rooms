import React from "react";
import { Flex, Box, Card, Pill } from "prisma-design-system";

/**
 * Hotels Details Page prototype
 * Test the layout for the hotel details page.
 *
 * This prototype has a single element flow in flexDirection _column_
 * Elements automatically fill the horizontal width due to the flex cross-axis logic.
 */
const PrototypeSingleColumn = ({ ...rest }) => {
  return (
    <>
      {/* Begin your prototype here */}
      <Flex
        flexDirection={"column"}
        flexRow={"nowrap"}
        style={{ border: "1px dashed gainsboro", padding: "4px" }}
      >
        <Box height={"33vh"} style={{ background: "LightSlateGray" }}>
          <mark>AccommodationImageSlideshow</mark>
          <Pill type={"informative"}>difficulty:medium</Pill>
          <Pill theme={"soft"} type={"positive"}>
            design:done
          </Pill>
        </Box>
        <Box style={{ marginTop: "-16px" }}>
          <Card px={4} py={6}>
            <Box style={{ background: "Lavender" }}>
              <Pill type={"positive"}>difficulty:easy</Pill>
              <Pill theme={"soft"} type={"warning"}>
                design:wip
              </Pill>
              <Flex
                mb={2}
                flexDirection={"column"}
                flexWrap={"wrap"}
                alignItems={"center"}
              >
                <Box mb={4}>
                  <mark>AccommodationName</mark>
                </Box>
                <Box mb={4}>
                  <Flex
                    px={2}
                    flexDirection={"row"}
                    flexWrap={"wrap"}
                    justifyContent={"center"}
                  >
                    <Box px={1} mb={2}>
                      <mark>AccommodationType</mark>
                    </Box>
                    <Flex justifyContent={"space-evenly"}>
                      <Box px={1}>
                        <mark>StarRating</mark>
                      </Box>
                      <Box px={1}>
                        <mark>TripAdvisor</mark>
                      </Box>
                    </Flex>
                    <Pill type={"negative"}>tripadvisor blocked by be</Pill>
                  </Flex>
                </Box>
              </Flex>
            </Box>
            <Box mb={6} style={{ minHeight: "175px", background: "Linen" }}>
              <Box>
                <mark>Description</mark>
              </Box>
              <Pill type={"positive"}>difficulty:easy</Pill>
              <Pill theme={"soft"} type={"positive"}>
                design:done
              </Pill>
            </Box>
            <Box mb={6} style={{ minHeight: "175px", background: "MistyRose" }}>
              <Box>
                <mark>Location</mark>
              </Box>
              <Pill type={"negative"}>difficulty:hard</Pill>
              <Pill theme={"soft"} type={"warning"}>
                design:wip
              </Pill>
            </Box>
            <Box mb={6} style={{ minHeight: "175px", background: "MintCream" }}>
              <Box>
                <mark>RoomSelectionList</mark>
              </Box>
              <Pill type={"warning"}>difficulty:medium</Pill>
              <Pill theme={"soft"} type={"warning"}>
                design:wip
              </Pill>
            </Box>
            <Box mb={6} style={{ minHeight: "175px", background: "SeaShell" }}>
              <Box>
                <mark>TripAdvisorReviews</mark>
              </Box>
              <Pill type={"negative"}>tripadvisor blocked by be</Pill>
              <Pill theme={"soft"} type={"warning"}>
                design:wip
              </Pill>
            </Box>
            <Box
              mb={6}
              style={{ minHeight: "175px", background: "WhiteSmoke" }}
            >
              <Box>
                <mark>AccommodationServices</mark>
              </Box>
              <Pill type={"warning"}>difficulty:medium</Pill>
              <Pill theme={"soft"} type={"positive"}>
                design:done
              </Pill>
            </Box>
            <Box mb={6} style={{ minHeight: "175px", background: "HoneyDew" }}>
              <Box>
                <mark>AccommodationConditions</mark>
              </Box>
              <Pill type={"positive"}>difficulty:easy</Pill>
              <Pill theme={"soft"} type={"positive"}>
                design:done
              </Pill>
            </Box>
          </Card>
        </Box>
      </Flex>
    </>
  );
};

PrototypeSingleColumn.propTypes = {};

export { PrototypeSingleColumn };
