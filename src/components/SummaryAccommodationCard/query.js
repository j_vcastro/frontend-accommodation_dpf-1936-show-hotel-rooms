import gql from "graphql-tag";

export const ACCOMMODATION_SUMMARY_QUERY = gql`
  query searchQuery(
    $accommodationSummaryRequest: AccommodationSummaryRequest!
  ) {
    accommodationSummary(
      accommodationSummaryRequest: $accommodationSummaryRequest
    ) {
      accommodationDetail {
        accommodationDealInformation {
          name
          address
          cityName
          description
          location
          chain
          type
          mainAccommodationImage {
            url
            thumbnailUrl
            type
            quality
          }
          cancellationPolicyDescription
          coordinates {
            latitude
            longitude
          }
          category
          paymentAtDestination
        }
        stateProvince
        country
        postalCode
        mapUrls
        locationDescription
        surroundingAreaInfo
        checkInPolicy
        checkOutPolicy
        hotelPolicy
        creditCardTypes
        paymentMethod
        phoneNumber
        mail
        web
        fax
        accommodationImages {
          url
          thumbnailUrl
          type
          quality
        }
        accommodationFacilities {
          facilityGroup {
            code
            description
          }
          code
          description
        }
        accommodationRoomServices {
          description
          type
        }
        numberOfRooms
        hotelType
      }
      roomDeals {
        description
        boardType
        bedsDescription
        isCancellationFree
      }
    }
  }
`;
