import React from "react";
import { axe } from "jest-axe";

import { cleanup } from "@testing-library/react";
import { renderAsync as render, TestingProviders } from "@testUtils";

import SummaryAccommodationCard from "../";
import { AccommodationSummaryResponse } from "../../../serverMocks";

const TRANSLATION = {
  accommodationCard: {
    isCancellationFree: "FREE_CANCELLATION"
  },
  accommodationTypes: {
    HOTEL: "HOTEL"
  },
  boardTypes: {
    AI: "ALL_INCLUSIVE"
  }
};

afterEach(cleanup);

describe("TranslationsTest", () => {
  test("It should render the Accommodation Card with mock data", async () => {
    const response = AccommodationSummaryResponse({
      accommodationDetail: {
        accommodationDealInformation: {
          name: "Hotel Name",
          cancellationFree: "TRUE"
        },
        hotelType: "HOTEL"
      },
      roomDeals: [
        {
          description: "Room description",
          boardType: "AI",
          bedsDescription: "Beds description",
          isCancellationFree: true,
          paymentAtDestination: true
        }
      ],
      roomsHaveSameFacilities: true
    });

    const { getByText } = await render(
      <TestingProviders translations={TRANSLATION} response={response}>
        <SummaryAccommodationCard />
      </TestingProviders>
    );

    expect(getByText("Hotel Name")).toBeVisible();
    expect(getByText("HOTEL")).toBeVisible();
    expect(getByText("ALL_INCLUSIVE")).toBeVisible();
    expect(getByText("Room description")).toBeVisible();
    expect(getByText("Beds description")).toBeVisible();
    expect(getByText("FREE_CANCELLATION")).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await render(
      <TestingProviders translations={TRANSLATION}>
        <SummaryAccommodationCard />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
