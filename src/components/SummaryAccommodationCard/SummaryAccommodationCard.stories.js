import React from "react";

import { AppProvidersDecorator } from "../../../.storybook/decorators";
import { AccommodationSummaryResponse } from "../../serverMocks";
import SummaryAccommodationCard from "./";

export default {
  title: "Components/Summary Accommodation Card",
  parameters: {
    componentSubtitle: "The accommodation card built with mock responses"
  }
};

const defaultResponse = (options = {}) => ({
  response: AccommodationSummaryResponse({
    accommodationDetail: {
      accommodationDealInformation: {
        name: "Hotel Name",
        mainAccommodationImage: {
          url:
            "https://www.edreams.com/pictures/3jq5xzwyo41l/2000000/1180000/1170200/1170158/05fe139a_z.jpg"
        },
        paymentAtDestination: true
      }
    },
    roomDeals: [
      {
        description: "Room description",
        boardType: "FB",
        bedsDescription: "Bed description",
        isCancellationFree: false
      }
    ]
  }),
  ...options
});

const cancellationFreeResponse = () => ({
  response: AccommodationSummaryResponse({
    accommodationDetail: {
      accommodationDealInformation: {
        name: "Hotel Name",
        cancellationFree: "TRUE",
        mainAccommodationImage: {
          url:
            "https://www.edreams.com/pictures/3jq5xzwyo41l/2000000/1180000/1170200/1170158/05fe139a_z.jpg"
        },
        paymentAtDestination: true
      }
    },
    roomDeals: [
      {
        description: "Room description 1",
        boardType: "BB",
        bedsDescription: "Bed description",
        isCancellationFree: true
      }
    ]
  })
});

const multipleRoomsWithDifferentFacilitiesResponse = () => ({
  response: AccommodationSummaryResponse({
    accommodationDetail: {
      accommodationDealInformation: {
        name: "Hotel Name",
        mainAccommodationImage: {
          url:
            "https://www.edreams.com/pictures/3jq5xzwyo41l/2000000/1180000/1170200/1170158/05fe139a_z.jpg"
        },
        paymentAtDestination: true
      },
      roomsHaveSameFacilities: true
    },
    roomDeals: [
      {
        description: "Room description 1",
        boardType: "BB",
        bedsDescription: "Bed description",
        isCancellationFree: true
      },
      {
        description: "Room description 2",
        boardType: "AI",
        bedsDescription: "Bed description",
        isCancellationFree: false
      }
    ]
  })
});

const multipleRoomsWithSameFacilitiesResponse = () => ({
  response: AccommodationSummaryResponse({
    accommodationDetail: {
      accommodationDealInformation: {
        name: "Hotel Name",
        mainAccommodationImage: {
          url:
            "https://www.edreams.com/pictures/3jq5xzwyo41l/2000000/1180000/1170200/1170158/05fe139a_z.jpg"
        },
        paymentAtDestination: true
      },
      roomsHaveSameFacilities: true
    },
    roomDeals: [
      {
        description: "Room description 1",
        boardType: "AI",
        bedsDescription: "Bed description 1",
        isCancellationFree: true
      },
      {
        description: "Room description 2",
        boardType: "AI",
        bedsDescription: "Bed description 2",
        isCancellationFree: true
      }
    ]
  })
});

const Template = args => <SummaryAccommodationCard {...args} />;

export const Default = Template.bind({});

export const CancellationFree = Template.bind({});

export const MultipleRoomsWithDifferentFacilities = Template.bind({});

export const MultipleRoomsWithSameFacilities = Template.bind({});

export const Loading = () => {
  return <SummaryAccommodationCard />;
};

Loading.decorators = [
  AppProvidersDecorator(defaultResponse({ loading: true }))
];
Default.decorators = [AppProvidersDecorator(defaultResponse())];

CancellationFree.decorators = [
  AppProvidersDecorator(cancellationFreeResponse())
];

CancellationFree.parameters = {
  docs: {
    description: {
      story: `A response with cancellationFree set to true`
    }
  }
};

MultipleRoomsWithDifferentFacilities.decorators = [
  AppProvidersDecorator(multipleRoomsWithDifferentFacilitiesResponse())
];

MultipleRoomsWithDifferentFacilities.parameters = {
  docs: {
    description: {
      story: `A response with an accommodation with multiple available rooms`
    }
  }
};

MultipleRoomsWithSameFacilities.decorators = [
  AppProvidersDecorator(multipleRoomsWithSameFacilitiesResponse())
];

MultipleRoomsWithSameFacilities.parameters = {
  docs: {
    description: {
      story: `A response with an accommodation with multiple available rooms`
    }
  }
};
