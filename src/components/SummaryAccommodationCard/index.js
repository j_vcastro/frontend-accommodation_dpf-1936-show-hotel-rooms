import React, { lazy, Suspense } from "react";
import { useAccommodationDetailParsedResponse } from "../../clientState/providers/SummaryAccommodationCardProviders/AccommodationDetailParsedResponseProvider";

import LoadingAccommodationCard from "@components/AccommodationCard/LoadingCard";

const AccommodationCard = lazy(() => import("../AccommodationCard"));

const SummaryAccommodationCard = () => {
  const { data, error, loading } = useAccommodationDetailParsedResponse();
  const { accommodationDetail, roomDeals, roomsHaveSameFacilities } =
    data || {};
  const accommodationInfo = {
    roomDeals,
    ...accommodationDetail,
    roomsHaveSameFacilities
  };

  if (loading) return <LoadingAccommodationCard />;
  if (!data || error) return null; // TODO: Track errors

  return (
    <Suspense fallback={<LoadingAccommodationCard />}>
      <AccommodationCard accommodationDetail={accommodationInfo} />
    </Suspense>
  );
};

export default SummaryAccommodationCard;
