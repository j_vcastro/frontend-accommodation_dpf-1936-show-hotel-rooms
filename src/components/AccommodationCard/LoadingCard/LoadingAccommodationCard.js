import React from "react";
import PropTypes from "prop-types";
import {
  LoadingCard,
  LoadingPlaceholder,
  LoadingButton,
  LoadingBox,
  Box,
  Flex
} from "prisma-design-system";

const sizes = ["small", "medium", "large"];
const LoadingSection = ({ length = 6, withButton = false }) => {
  return (
    <Flex flexDirection="column" flex={1} justifyContent="space-between">
      {Array.from({ length }).map((_, index) => {
        return (
          <Box key={index} mb={4} width="100%">
            <LoadingPlaceholder size={sizes[index % 3]} />
          </Box>
        );
      })}
      {withButton && (
        <Flex justifyContent="flex-end">
          <LoadingButton />
        </Flex>
      )}
    </Flex>
  );
};

/** Loading accommodation card, demonstrates a responsive layout: one column in small container, two columns in medium, and then three columns in a large one */
const LoadingAccommodationCard = ({ showPriceSection }) => {
  const firstColFlexBasis = "1 1 150px",
    secondColFlexBasis = "5 1 500px",
    minHeight = "150px";
  return (
    <LoadingCard animationDelay={-1} p={0}>
      <Flex flexDirection="row" flexWrap="wrap" minHeight="250px">
        <Flex flex={`${firstColFlexBasis}`} alignItems="stretch">
          <LoadingBox width="100%" minHeight={minHeight} />
        </Flex>
        <Flex flex={`${secondColFlexBasis}`}>
          <Flex flexGrow={1} flexDirection="row" flexWrap="wrap">
            <Flex
              flex="3 1 250px"
              alignItems="stretch"
              minHeight={`${minHeight}`}
              p={4}
            >
              <LoadingSection />
            </Flex>
            {showPriceSection ? (
              <Flex
                flex="1 1 250px"
                alignItems="stretch"
                minHeight={`${minHeight}`}
                p={4}
              >
                <LoadingSection withButton={true} />
              </Flex>
            ) : null}
          </Flex>
        </Flex>
      </Flex>
    </LoadingCard>
  );
};

LoadingAccommodationCard.propTypes = {
  showPriceSection: PropTypes.bool
};

LoadingAccommodationCard.defaultProps = {
  showPriceSection: false
};

export default LoadingAccommodationCard;
