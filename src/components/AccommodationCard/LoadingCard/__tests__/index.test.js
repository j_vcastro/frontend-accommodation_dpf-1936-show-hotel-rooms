import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import LoadingAccommodationCard from "../index";

const TEST = "test";

const TRANSLATION = {
  filters: TEST
};

afterEach(cleanup);

describe("LoadingAccommodationCard", () => {
  test("should not have basic accessibility issues", async () => {
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <LoadingAccommodationCard />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
