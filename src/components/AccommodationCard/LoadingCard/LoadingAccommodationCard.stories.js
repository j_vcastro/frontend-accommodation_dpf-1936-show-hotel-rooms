import React from "react";
import { Box } from "prisma-design-system";
import LoadingAccommodationCard from "./";

export default {
  title: "Components/Accommodation Card/Loading Card",
  component: LoadingAccommodationCard,
  args: {
    showPriceSection: true
  }
};

const Template = args => {
  return <LoadingAccommodationCard {...args} />;
};

export const Small = Template.bind({ showPriceSection: true });
Small.decorators = [Story => <Box width="300px">{Story()}</Box>];

export const Large = Template.bind({ showPriceSection: true });
Large.decorators = [Story => <Box width="768px">{Story()}</Box>];

export const Medium = Template.bind({ showPriceSection: true });
Medium.decorators = [Story => <Box width="500px">{Story()}</Box>];
