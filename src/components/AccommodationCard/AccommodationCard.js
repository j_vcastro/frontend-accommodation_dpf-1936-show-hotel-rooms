import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  Flex,
  FlexRow,
  Col,
  HighlightedCard
} from "prisma-design-system";

import AccommodationImage from "./AccommodationImage";
import AccommodationName from "./AccommodationName";
import AccommodationType from "@components/AccommodationType";
import AccommodationAddress from "./AccommodationAddress";
import AccommodationHeader from "@components/AccommodationCard/AccommodationHeader";
import StarRating from "@components/StarRating";
import Rooms from "@components/AccommodationCard/Rooms";

/** AccommodationCard displays an overview of _one_ accommodation. Uses the Prisma Card component. */
const AccommodationCard = ({
  accommodationDetail,
  isHighlighted,
  showPrice
}) => {
  const {
    accommodationDealInformation,
    hotelType,
    roomDeals,
    roomsHaveSameFacilities
  } = accommodationDetail;
  const {
    name,
    address,
    category,
    mainAccommodationImage
  } = accommodationDealInformation;

  const CardComponent = isHighlighted ? HighlightedCard : Card;

  // Style props
  const cardMinHeight = "250px",
    firstColFlexBasis = "1 1 150px",
    secondColFlexBasis = "5 1 500px",
    infoMinWidth = "250px",
    infoMinHeight = "150px",
    priceMinWidth = "250px",
    priceMinHeight = "150px",
    imageMinHeight = "150px";

  return (
    <>
      {isHighlighted && <AccommodationHeader />}
      <CardComponent p={0} width="100%" type={isHighlighted ? "positive" : ""}>
        <Flex flexWrap={"wrap"} minHeight={`${cardMinHeight}`}>
          {/* The first column, contains the image */}
          <Flex
            flex={`${firstColFlexBasis}`}
            alignItems={"stretch"}
            minHeight={`${imageMinHeight}`}
          >
            <AccommodationImage
              imageUrl={
                mainAccommodationImage &&
                mainAccommodationImage.url &&
                mainAccommodationImage.url
              }
              altText={name}
            />
          </Flex>
          {/* The second column, contains the info and the price */}
          <Flex flex={`${secondColFlexBasis}`}>
            <Flex flexGrow={1} flexWrap="wrap">
              <Flex
                flex={`3 1 ${infoMinWidth}`}
                alignItems={"stretch"}
                minHeight={`${infoMinHeight}`}
              >
                {/* The info, this is where you can split out to a child AccommodationInfo component */}
                <>
                  <Flex flex={1} flexDirection="column" p={4}>
                    <AccommodationName name={name} />
                    <FlexRow alignItems={"center"} gap={2} pb={2}>
                      <Col>
                        <AccommodationType type={hotelType} />
                      </Col>
                      <Col>
                        <StarRating category={category} />
                      </Col>
                    </FlexRow>
                    <AccommodationAddress address={address} />
                    <Rooms
                      roomDeals={roomDeals}
                      roomsHaveSameFacilities={roomsHaveSameFacilities}
                    />
                  </Flex>
                </>
              </Flex>
              {showPrice && (
                <Flex
                  flex={`1 1 ${priceMinWidth}`}
                  alignItems={"stretch"}
                  minHeight={`${priceMinHeight}`}
                >
                  {/* The price, this is where you can split out to a child AccommodationPrice component */}
                  <>
                    <mark>The price</mark>
                  </>
                </Flex>
              )}
            </Flex>
          </Flex>
        </Flex>
      </CardComponent>
    </>
  );
};

AccommodationCard.defaultProps = {
  isHighlighted: false,
  showPrice: false
};

AccommodationCard.propTypes = {
  accommodationDetail: PropTypes.object,
  /** Whether to render in a HighlightedCard component or not. Default is false. !This prop can be removed in future, and set it based on JSON data! */
  isHighlighted: PropTypes.bool,
  /** Whether to render the price column in the component. Default is false. !This prop can be removed in future, and we can render based on JSON data! */
  showPrice: PropTypes.bool,
  roomDeals: PropTypes.object
};

export default AccommodationCard;
