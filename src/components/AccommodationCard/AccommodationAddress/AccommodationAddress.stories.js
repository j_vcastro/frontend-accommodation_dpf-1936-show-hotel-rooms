import React from "react";
import AccommodationAddress from "./";

export default {
  title: "Components/Accommodation Card/Accommodation Address",
  component: AccommodationAddress
};

export const Default = args => {
  return <AccommodationAddress {...args} />;
};
Default.args = {
  address: "Example Accommodation Address"
};
