import React from "react";
import PropTypes from "prop-types";
import { Box, Text } from "prisma-design-system";
import { useTranslation } from "@frontend-shell/react-hooks";

const AccommodationAddress = ({ address }) => {
  const { t } = useTranslation();
  const addressLabel = t(`accommodationCard.addressLabel`);
  return (
    <Box pb={2}>
      <Text priority={5} fontSize="body.0">
        {addressLabel} {address}
      </Text>
    </Box>
  );
};

AccommodationAddress.propTypes = {
  address: PropTypes.string.isRequired
};

export default AccommodationAddress;
