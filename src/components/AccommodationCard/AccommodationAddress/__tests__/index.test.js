import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import AccommodationAddress from "../";

const ADDRESS_LABEL = "Address:";

const TRANSLATION = {
  accommodationCard: {
    addressLabel: ADDRESS_LABEL
  }
};

afterEach(cleanup);

describe("AccommodationAddress", () => {
  test("It should render their children", async () => {
    const address = "text example";
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <AccommodationAddress address={address} />
      </TestingProviders>
    );
    const outputText = `${ADDRESS_LABEL} ${address}`;
    expect(getByText(outputText)).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const content = "text example";
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <AccommodationAddress address={content} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
