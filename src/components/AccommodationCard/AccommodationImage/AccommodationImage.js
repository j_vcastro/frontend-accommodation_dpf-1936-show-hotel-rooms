import React from "react";
import PropTypes from "prop-types";
import { BackgroundImage } from "prisma-design-system";

/**
 * Displays a single image using the
 * __[BackgroundImage](http://lb.prisma-design-system.gke-apps.edo.sv/?path=/story/components-backgroundimage--base)__
 * Prisma component.
 */
const AccommodationImage = ({ placeholderUrl, imageUrl, altText }) => {
  let backgroundImageUrl = imageUrl ? `url('${imageUrl}'), ` : "";
  backgroundImageUrl += `url('${placeholderUrl}')`;

  return (
    <BackgroundImage
      alt={altText}
      backgroundImage={backgroundImageUrl}
      backgroundSize={"cover"}
    />
  );
};

AccommodationImage.defaultProps = {
  placeholderUrl:
    "https://www.edreams.com/images/onefront/bluestone/ED/no-hotel-pic.png"
};

AccommodationImage.propTypes = {
  /** The placeholder image; this is displayed when the image is loading, or when the image cannot be found. It has a default value. */
  placeholderUrl: PropTypes.string,
  /** The URL of the image; it will be displayed using a BackgroundImage component from Prisma. */
  imageUrl: PropTypes.string,
  /** The image alt text */
  altText: PropTypes.string
};

export default AccommodationImage;
