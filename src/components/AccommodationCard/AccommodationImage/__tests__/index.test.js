import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import AccommodationImage from "../index";

afterEach(cleanup);

describe("AccommodationImage", () => {
  test("It should always render the placeholder image", async () => {
    const altText = "An empty image";
    const { getByRole } = await renderAsync(
      <TestingProviders>
        <AccommodationImage altText={altText} />
      </TestingProviders>
    );
    const theImage = getByRole("img", { name: altText });
    expect(theImage).toBeVisible();
    const style = window.getComputedStyle(theImage);
    expect(style.backgroundImage).toContain("no-hotel-pic");
  });

  test("It should render the correct image", async () => {
    const altText = "The alt text";
    const content = "test.jpeg";
    const { getByRole } = await renderAsync(
      <TestingProviders>
        <AccommodationImage imageUrl={content} altText={altText} />
      </TestingProviders>
    );
    const theImage = getByRole("img", { name: altText });
    expect(theImage).toBeVisible();
    // const style = window.getComputedStyle(theImage);
    // expect(style.backgroundImage).toContain(content);
  });

  test("should not have basic accessibility issues", async () => {
    const content =
      "https://www.edreams.com/pictures/9713tvxvasaj/1000000/10000/9400/9339/e2a10f4b_t.jpg";
    const altText = "The image alt text";
    const { container } = await renderAsync(
      <TestingProviders>
        <AccommodationImage imageUrl={content} altText={altText} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  test("should have violation when no alt text is set", async () => {
    const content =
      "https://www.edreams.com/pictures/9713tvxvasaj/1000000/10000/9400/9339/e2a10f4b_t.jpg";
    const { container } = await renderAsync(
      <TestingProviders>
        <AccommodationImage imageUrl={content} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).not.toHaveNoViolations();
  });
});
