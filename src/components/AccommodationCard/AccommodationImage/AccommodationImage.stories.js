import React from "react";
import AccommodationImage from "./index";
import { Flex } from "prisma-design-system";

export default {
  title: "Components/Accommodation Card/Accommodation Image",
  component: AccommodationImage
};

const Template = ({ ...args }) => {
  return (
    <Flex
      width="900px"
      maxWidth="90vw"
      height="600px"
      maxHeight="90vh"
      style={{ border: "2px dashed orange", padding: "2px" }}
    >
      <AccommodationImage {...args} />
    </Flex>
  );
};

export const Default = Template.bind({});

Default.args = {
  imageUrl:
    "https://www.edreams.com/pictures/9713tvxvasaj/1000000/10000/9400/9339/e2a10f4b_z.jpg"
};

export const EmptyImage = Template.bind({});
