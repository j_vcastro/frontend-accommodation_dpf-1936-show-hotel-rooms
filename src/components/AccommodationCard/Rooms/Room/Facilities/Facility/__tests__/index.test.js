import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import Facility from "../";

const TRANSLATION = {
  accommodationCard: {
    paymentAtDestination: "PAY_LATER",
    isCancellationFree: "FREE_HOTEL_CANCELLATION"
  }
};

afterEach(cleanup);

describe("Facility", () => {
  test("It should render the pay later message", async () => {
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facility type="paymentAtDestination" value={true} />
      </TestingProviders>
    );

    expect(getByText("PAY_LATER")).toBeVisible();
  });

  test("It should not render the pay later message", async () => {
    const { queryByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facility type="paymentAtDestination" value={false} />
      </TestingProviders>
    );

    expect(queryByText("PAY_LATER")).toBeNull();
  });

  test("It should render the free hotel cancellation message", async () => {
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facility type="isCancellationFree" value={true} />
      </TestingProviders>
    );

    expect(getByText("FREE_HOTEL_CANCELLATION")).toBeVisible();
  });

  test("It should not render the free hotel cancellation message", async () => {
    const { queryByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facility type="isCancellationFree" value={false} />
      </TestingProviders>
    );

    expect(queryByText("FREE_HOTEL_CANCELLATION")).toBeNull();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facility type="isCancellationFree" value={true} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
