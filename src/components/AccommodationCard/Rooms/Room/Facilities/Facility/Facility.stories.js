import React from "react";
import Facility from "./";

export default {
  title: "Components/Accommodation Card/Rooms/Room/Facilities/Facility",
  component: Facility
};

export const Default = args => {
  return <Facility {...args} />;
};

Default.args = {
  type: "paymentAtDestination",
  value: true
};
