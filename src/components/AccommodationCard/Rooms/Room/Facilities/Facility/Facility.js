import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "@frontend-shell/react-hooks";
import { Box, CheckIcon } from "prisma-design-system";
import Tag from "@components/Tag";

const Facility = ({ type, value }) => {
  const { t } = useTranslation();
  return (
    value && (
      <Box mt={2}>
        <Tag mt={2} type="positive" icon={<CheckIcon size="small" />}>
          {t(`accommodationCard.${type}`)}
        </Tag>
      </Box>
    )
  );
};

const validTypes = ["paymentAtDestination", "isCancellationFree"];

Facility.propTypes = {
  type: PropTypes.oneOf(validTypes),
  value: PropTypes.bool
};
export default Facility;
