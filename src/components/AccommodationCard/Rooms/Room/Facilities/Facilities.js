import React from "react";
import PropTypes from "prop-types";
import { Col, FlexRow } from "prisma-design-system";
import Facility from "./Facility";
import BoardType from "./BoardType";

const Facilities = ({ facilities }) => (
  <FlexRow gap={4} flexWrap="wrap">
    {facilities &&
      facilities.map(
        ({ type, value }, index) =>
          value && (
            <Col key={index} flexGrow={0}>
              {type === "boardType" ? (
                <BoardType type={value} />
              ) : (
                <Facility type={type} value={value} />
              )}
            </Col>
          )
      )}
  </FlexRow>
);

Facilities.propTypes = {
  facilities: PropTypes.array.isRequired
};

export default Facilities;
