import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import Facilities from "../";

const TEST = "test";

const TRANSLATION = {
  filters: TEST
};

const facilities = [
  {
    type: "boardType",
    value: "AI"
  },
  {
    type: "paymentAtDestination",
    value: true
  },
  {
    type: "isCancellationFree",
    value: true
  }
];

afterEach(cleanup);

describe("Facilities", () => {
  test("It should render their children", async () => {
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facilities facilities={facilities} />
      </TestingProviders>
    );

    expect(getByText("boardTypes.AI")).toBeVisible();
    expect(getByText("accommodationCard.paymentAtDestination")).toBeVisible();
    expect(getByText("accommodationCard.isCancellationFree")).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Facilities facilities={facilities} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
