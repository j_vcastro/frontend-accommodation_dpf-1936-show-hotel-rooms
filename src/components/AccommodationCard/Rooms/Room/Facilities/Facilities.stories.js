import React from "react";
import Facilities from "./";

export default {
  title: "Components/Accommodation Card/Rooms/Room/Facilities",
  component: Facilities
};

export const Default = args => {
  return <Facilities {...args} />;
};

Default.args = {
  facilities: [
    {
      type: "boardType",
      value: "AI"
    },
    {
      type: "paymentAtDestination",
      value: true
    },
    {
      type: "isCancellationFree",
      value: true
    }
  ]
};
