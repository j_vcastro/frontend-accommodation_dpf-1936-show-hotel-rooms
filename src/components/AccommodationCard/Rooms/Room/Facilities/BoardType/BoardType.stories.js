import React from "react";
import BoardType from "./index";

export default {
  title: "Components/Accommodation Card/Rooms/Room/Facilities/Board Type",
  component: BoardType
};

const Template = args => <BoardType {...args} />;

export const Default = Template.bind({});

Default.args = {
  type: "AI"
};
