import React from "react";
import PropTypes from "prop-types";
import Tag from "@components/Tag";
import { useTranslation } from "@frontend-shell/react-hooks";
import {
  Box,
  CoffeeCupIcon,
  ForkKnifeIcon,
  MicrowaveIcon
} from "prisma-design-system";

const iconMapper = {
  AI: ForkKnifeIcon,
  BB: CoffeeCupIcon,
  FB: ForkKnifeIcon,
  HB: ForkKnifeIcon,
  SC: MicrowaveIcon
};

const BoardType = ({ type }) => {
  const { t } = useTranslation();
  const boardTypeText = t(`boardTypes.${type}`);
  const IconComponent = iconMapper[type];

  return boardTypeText && IconComponent ? (
    <Box mt={2}>
      <Tag mt={2} type="positive" icon={<IconComponent size="small" />}>
        {boardTypeText}
      </Tag>
    </Box>
  ) : null;
};

const validTypes = ["AI", "BB", "FB", "HB", "RO", "SC", "UN"];

BoardType.propTypes = {
  type: PropTypes.oneOf(validTypes)
};

export default BoardType;
