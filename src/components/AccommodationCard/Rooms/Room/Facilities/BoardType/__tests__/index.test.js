import React from "react";
import { cleanup } from "@testing-library/react";
import { renderAsync, TestingProviders } from "@testUtils";
import BoardType from "../index";

const TRANSLATION = {
  boardTypes: {
    AI: "ALL_INCLUSIVE",
    UN: "UNKNOWN",
    RO: "ROOM_ONLY"
  }
};

afterEach(cleanup);

describe("BoardType", () => {
  test("It should show the all inclusive (AI) board type ", async () => {
    const type = "AI";
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <BoardType type={type} />
      </TestingProviders>
    );

    expect(getByText("ALL_INCLUSIVE")).toBeVisible();
  });

  test("It should not show the unknown (UN) board type", async () => {
    const type = "UN";
    const { queryByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <BoardType type={type} />
      </TestingProviders>
    );

    expect(queryByText("UNKNOWN")).toBeNull();
  });

  test("It should not show the room only (RO) board type", async () => {
    const type = "RO";
    const { queryByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <BoardType type={type} />
      </TestingProviders>
    );

    expect(queryByText("ROOM_ONLY")).toBeNull();
  });
});
