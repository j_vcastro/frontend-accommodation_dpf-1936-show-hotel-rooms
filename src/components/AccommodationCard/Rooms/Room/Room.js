import React from "react";
import { Flex, FlexRow, Text } from "prisma-design-system";
import Facilities from "./Facilities";
import PropTypes from "prop-types";

const getFacilities = (boardType, paymentAtDestination, isCancellationFree) => [
  {
    type: "boardType",
    value: boardType
  },
  {
    type: "paymentAtDestination",
    value: paymentAtDestination
  },
  {
    type: "isCancellationFree",
    value: isCancellationFree
  }
];

const Room = ({
  description,
  bedsDescription,
  showFacilities,
  boardType,
  paymentAtDestination,
  isCancellationFree
}) => (
  <Flex flexDirection="column" mt={4}>
    <FlexRow pb={2}>
      <Text fontSize="body.1" fontWeight="bold" color="neutrals.1">
        {description}
      </Text>
    </FlexRow>
    <FlexRow pb={1}>
      <Text fontSize="body.0" color="neutrals.2">
        {bedsDescription}
      </Text>
    </FlexRow>
    {showFacilities && (
      <Facilities
        facilities={getFacilities(
          boardType,
          paymentAtDestination,
          isCancellationFree
        )}
      />
    )}
  </Flex>
);

const validBoardTypes = ["AI", "BB", "FB", "HB", "RO", "SC", "UN"];

Room.propTypes = {
  description: PropTypes.string,
  bedsDescription: PropTypes.string,
  boardType: PropTypes.oneOf(validBoardTypes)
};

Room.defaultTypes = {
  showFacilities: true
};

export default Room;
