import React from "react";
import Room from "./index";
import { Box } from "prisma-design-system";

export default {
  title: "Components/Accommodation Card/Rooms/Room",
  component: Room,
  args: {
    description: "Premium Room",
    boardType: "FB",
    bedsDescription: "Double bed",
    paymentAtDestination: true,
    isCancellationFree: true,
    showFacilities: true
  }
};

export const Default = args => {
  return <Room {...args} />;
};

export const MultilineFacilities = args => {
  return <Room {...args} />;
};

MultilineFacilities.args = {
  description: "Executive Standard Room",
  boardType: "AI",
  bedsDescription: "3 standards beds",
  isCancellationFree: true,
  paymentAtDestination: true,
  showFacilities: true
};

MultilineFacilities.decorators = [Story => <Box width="300px">{Story()}</Box>];
