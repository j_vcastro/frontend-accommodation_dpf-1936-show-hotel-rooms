import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { TestingProviders, renderAsync } from "@testUtils";
import Room from "../index";

const TEST = "test";
const roomDeal = {
  description: "Room description",
  boardType: "FB",
  bedsDescription: "Beds description",
  showFacilities: true
};

const TRANSLATION = {
  filters: TEST,
  boardTypes: {
    FB: "FULL_BOARD"
  }
};

afterEach(cleanup);

describe("Room", () => {
  test("It should render their children", async () => {
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Room {...roomDeal} />
      </TestingProviders>
    );

    expect(getByText("Room description")).toBeVisible();
    expect(getByText("Beds description")).toBeVisible();
    expect(getByText("FULL_BOARD")).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Room {...roomDeal} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
