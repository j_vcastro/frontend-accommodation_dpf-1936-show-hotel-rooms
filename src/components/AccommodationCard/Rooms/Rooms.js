import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Room from "./Room";
import Facilities from "./Room/Facilities";

const getFacilities = (paymentAtDestination, isCancellationFree, boardType) => [
  {
    type: "paymentAtDestination",
    value: paymentAtDestination
  },
  {
    type: "isCancellationFree",
    value: isCancellationFree
  },
  {
    type: "boardType",
    value: boardType
  }
];

const Rooms = ({ roomDeals, roomsHaveSameFacilities }) => (
  <Fragment>
    {roomDeals &&
      roomDeals.map((roomDeal, index) => (
        <Room
          key={index}
          showFacilities={!roomsHaveSameFacilities}
          {...roomDeal}
        />
      ))}
    {roomsHaveSameFacilities && (
      <Facilities
        facilities={getFacilities(
          roomDeals[0].paymentAtDestination,
          roomDeals[0].isCancellationFree,
          roomDeals[0].boardType
        )}
        mt={4}
      />
    )}
  </Fragment>
);

Rooms.propTypes = {
  roomDeals: PropTypes.array.isRequired,
  roomsHaveSameFacilities: PropTypes.bool.isRequired
};

export default Rooms;
