import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import Rooms from "../";

const TRANSLATION = {
  boardTypes: {
    BB: "BREAKFAST_INCLUDED"
  },
  paymentAtDestination: true
};

afterEach(cleanup);

const props = {
  roomDeals: [
    {
      description: "Executive Standard Room",
      boardType: "BB",
      bedsDescription: "3 standards beds",
      isCancellationFree: true,
      paymentAtDestination: true
    },
    {
      description: "Executive Standard Room",
      boardType: "FB",
      bedsDescription: "3 standards beds",
      isCancellationFree: true,
      paymentAtDestination: true
    }
  ],
  roomsHaveSameFacilities: true,
  paymentAtDestination: true,
  isCancellationFree: true
};

describe("Rooms", () => {
  test("It should render their children", async () => {
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Rooms {...props} />
      </TestingProviders>
    );

    expect(getByText("BREAKFAST_INCLUDED")).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Rooms {...props} />
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
