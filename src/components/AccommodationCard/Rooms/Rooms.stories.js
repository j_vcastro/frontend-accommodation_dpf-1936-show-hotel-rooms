import React from "react";
import Rooms from "./";

export default {
  title: "Components/Accommodation Card/Rooms",
  component: Rooms
};

export const Default = args => {
  return <Rooms {...args} />;
};
Default.args = {
  roomDeals: [
    {
      description: "Executive Standard Room",
      boardType: "BB",
      bedsDescription: "3 standards beds",
      isCancellationFree: true,
      paymentAtDestination: true
    },
    {
      description: "Premium Room",
      boardType: "AI",
      bedsDescription: "3 standards beds",
      isCancellationFree: false,
      paymentAtDestination: true
    }
  ],
  roomsHaveSameFacilities: true
};
