import React from "react";
import AccommodationName from "./";

export default {
  title: "Components/Accommodation Card/Accommodation Name",
  component: AccommodationName
};

export const Default = args => {
  return <AccommodationName {...args} />;
};

Default.args = {
  name: "Example Accommodation Name"
};
