import React from "react";
import PropTypes from "prop-types";
import { Box, Text } from "prisma-design-system";

const AccommodationName = ({ name }) => (
  <Box pb={2}>
    <Text fontSize="body.3" fontWeight="medium" lineHeight="body">
      {name}
    </Text>
  </Box>
);

AccommodationName.propTypes = {
  name: PropTypes.string.isRequired
};

export default AccommodationName;
