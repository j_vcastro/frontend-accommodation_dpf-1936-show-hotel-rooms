import React from "react";
import { cleanup } from "@testing-library/react";
import { renderAsync, TestingProviders } from "@testUtils";
import AccommodationName from "../";

afterEach(cleanup);

describe("AccommodationName", () => {
  test("It should show the accommodation name", async () => {
    const content = "example name";
    const { getByText } = await renderAsync(
      <TestingProviders>
        <AccommodationName name={content} />
      </TestingProviders>
    );

    expect(getByText(content)).toBeVisible();
  });
});
