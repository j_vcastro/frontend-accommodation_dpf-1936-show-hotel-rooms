import React from "react";
import AccommodationCard from "./";
import defaultData from "./mockAccommodationDetail.json";

export default {
  title: "Components/Accommodation Card",
  component: AccommodationCard,
  argTypes: {
    containerWidth: {
      description:
        "the width of the parent element. note this has a max width of 100vw of the viewport (so that mobile previews work ok)",
      control: "text"
    },
    accommodationDetail: {
      description:
        "Object used to render the AccommodationCard. This story uses a static JSON file. If you want to see how the card works with a mock _provider_ then look at the SummaryAccommodationCard stories."
    },
    roomDeals: {
      description:
        "Array used to render the room options for a certain hotel. This story used a static JSON file Refer to the Room Component story to see how the component behaves with a mock provider"
    }
  }
};

const DefaultTemplate = ({ containerWidth, ...args }) => {
  return (
    <>
      <div style={{ width: `${containerWidth}`, maxWidth: "100vw" }}>
        <AccommodationCard {...args} />
      </div>
    </>
  );
};

export const Default = DefaultTemplate.bind({});
Default.args = {
  containerWidth: "100%",
  accommodationDetail: defaultData[0]
};

const CustomDataTemplate = ({
  containerWidth,
  name,
  address,
  cancellationFree,
  hotelType,
  category,
  url,
  description,
  bedsDescription,
  boardType,
  ...rest
}) => {
  let accommodationDetail = {
    hotelType,
    accommodationDealInformation: {
      name,
      address,
      cancellationFree,
      category,
      mainAccommodationImage: { url },
      paymentAtDestination: true
    },
    roomDeals: [
      {
        description,
        bedsDescription,
        boardType
      }
    ]
  };
  return (
    <>
      <div style={{ width: `${containerWidth}`, maxWidth: "100vw" }}>
        <AccommodationCard
          accommodationDetail={accommodationDetail}
          {...rest}
        />
      </div>
    </>
  );
};

export const WithCustomData = CustomDataTemplate.bind({});
WithCustomData.argTypes = {
  cancellationFree: {
    control: {
      type: "select"
    },
    options: ["TRUE", "FALSE"]
  },
  hotelType: {
    control: {
      type: "select"
    },
    options: [
      "APARTAMENT",
      "BED_AND_BREAKFAST",
      "GUEST_HOUSE",
      "HOSTEL",
      "HOTEL",
      "MOTEL",
      "RESIDENCE",
      "RESORT",
      "RYOKAN",
      "UNKNOWN"
    ]
  },
  category: {
    description: "The hotel star rating",
    control: {
      type: "select"
    },
    options: [
      "UNKNOWN",
      "STARS_0",
      "STARS_0_5",
      "STARS_1",
      "STARS_1_5",
      "STARS_2",
      "STARS_2_5",
      "STARS_3",
      "STARS_3_5",
      "STARS_4",
      "STARS_4_5",
      "STARS_5",
      "STARS_6",
      "STARS_7"
    ]
  },
  boardType: {
    description: "The room's board type",
    control: {
      type: "select"
    },
    options: ["AI", "BB", "FB", "HB", "RO", "SC", "UN"]
  }
};
WithCustomData.args = {
  containerWidth: "100%",
  name: "Bates motel",
  address: "Carrer Bailen 99",
  cancellationFree: "TRUE",
  hotelType: "BED_AND_BREAKFAST",
  category: "STARS_3",
  url:
    "https://www.edreams.com/pictures/3jq5xzwyo41l/2000000/1180000/1170200/1170158/05fe139a_z.jpg",
  description: "Double room",
  bedsDescription: "Two double beds",
  boardType: "AI"
};
