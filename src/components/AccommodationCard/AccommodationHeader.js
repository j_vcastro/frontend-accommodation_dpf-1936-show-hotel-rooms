import React from "react";
import { Flex, HighlightedHeader } from "prisma-design-system";

const HighlightedMessage = () => (
  <Flex alignItems="center">{"Highlighted"}</Flex>
);

const AccommodationHeader = () => {
  return (
    <HighlightedHeader type="positive" size="full">
      <HighlightedMessage />
    </HighlightedHeader>
  );
};

export default AccommodationHeader;
