import React from "react";
import { Flex } from "prisma-design-system";

/**
 * the Accommodation Details component contains detailed information about a single accommodation.
 */
const AccommodationDetails = () => {
  return (
    <Flex flexDirection={"column"} flexRow={"nowrap"}>
      {/* add components here */}
    </Flex>
  );
};

AccommodationDetails.propTypes = {};

export default AccommodationDetails;
