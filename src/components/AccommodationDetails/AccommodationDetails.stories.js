import React from "react";
import AccommodationDetails from "./";

export default {
  title: "Components/Accommodation Details",
  component: AccommodationDetails,
  argTypes: {
    containerWidth: {
      description:
        "the width of the parent element. note this has a max width of 100vw of the viewport (so that mobile previews work ok)",
      control: "text"
    }
  }
};

const Template = ({ containerWidth, ...args }) => {
  return (
    <>
      <div style={{ width: `${containerWidth}`, maxWidth: "100vw" }}>
        <AccommodationDetails {...args} />
      </div>
    </>
  );
};

export const Default = Template.bind({});
Default.args = {
  containerWidth: "100%"
};
