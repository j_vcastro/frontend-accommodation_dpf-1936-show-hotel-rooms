import React from "react";
import ImageSlideShow from "./index";
import { Box } from "prisma-design-system";
export default {
  title: "Components/Image Slide Show",
  component: ImageSlideShow
};

const images = [
  {
    url:
      "https://www.edreams.com/pictures/2e9ftwwuru0e/1000000/600000/597700/597669/b3216c7b_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/1g46uuwy8cgb/1000000/600000/597700/597669/b3216c7b_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/4377xuzy6ux9/1000000/600000/597700/597669/cc0a6b63_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/1ov3tyuw1g1r/1000000/600000/597700/597669/cc0a6b63_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/6rwtuzxt8hh7/1000000/600000/597700/597669/4434ed78_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/u8b7uxvwu589/1000000/600000/597700/597669/4434ed78_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/caf6uxvxl5no/1000000/600000/597700/597669/55fc9cca_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/2f4jvtvy95mh/1000000/600000/597700/597669/55fc9cca_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/du32xttzi44g/1000000/600000/597700/597669/d33f0afe_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/a6aczvtxj9b2/1000000/600000/597700/597669/d33f0afe_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/nq6duwuyt7w2/1000000/600000/597700/597669/c0244a0a_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/ccm1vytw4e5x/1000000/600000/597700/597669/c0244a0a_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/rn7rwuxy50sr/1000000/600000/597700/597669/84c66fcf_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/gn49xztvjfi3/1000000/600000/597700/597669/84c66fcf_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/v14cvvvvcok2/1000000/600000/597700/597669/68073bfd_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/7609zyzw168r/1000000/600000/597700/597669/68073bfd_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/v2vmyzuyzzr1/1000000/600000/597700/597669/538307a1_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/n454zxyu1faa/1000000/600000/597700/597669/538307a1_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  },
  {
    url:
      "https://www.edreams.com/pictures/x6b9xxwti026/1000000/600000/597700/597669/5748b635_z.jpg",
    thumbnailUrl:
      "https://www.edreams.com/pictures/4pm9wwyvumtd/1000000/600000/597700/597669/5748b635_t.jpg",
    type: "HOTEL",
    quality: "MEDIUM"
  }
];
export const Default = args => {
  return (
    <Box width="500px" height="300px">
      <ImageSlideShow {...args} />
    </Box>
  );
};
Default.args = {
  images
};
