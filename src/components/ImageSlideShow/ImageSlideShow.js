import React from "react";
import PropTypes from "prop-types";
import { Slide, SlideShow, BackgroundImage } from "prisma-design-system";

const ImageSlideShow = ({ showControls, images }) => {
  return (
    <SlideShow
      controls={showControls}
      prevButtonAriaLabel="Previous Image"
      nextButtonAriaLabel="Next Image"
      navigationAriaLabelFunction={(index, total) =>
        `Image ${index + 1} of ${total} images of this place`
      }
    >
      {images.map((image, index) => (
        <Slide key={index}>
          <BackgroundImage
            width="100%"
            height="100%"
            backgroundImage={`url(${image.url})`}
            backgroundSize="cover"
            alt=""
          />
        </Slide>
      ))}
    </SlideShow>
  );
};

ImageSlideShow.propTypes = {
  images: PropTypes.array,
  showControls: PropTypes.bool
};

export default ImageSlideShow;
