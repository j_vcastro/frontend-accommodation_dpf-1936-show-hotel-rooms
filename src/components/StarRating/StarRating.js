import React from "react";
import PropTypes from "prop-types";
import { Flex, Rating } from "prisma-design-system";

/**
 * The Accommodation Star Rating, uses the
 * [Rating](http://lb.prisma-design-system.gke-apps.edo.sv/?path=/story/components-rating--base) component from Prisma.<br/>
 * The incoming category prop __must__ be in the format STARS\_NUM. for example: "STARS\_3".
 */
const StarRating = ({ category }) => {
  const amount = parseFloat(category.substring(6).replace("_", "."));

  if (isNaN(amount)) {
    return null;
  }

  return (
    <Flex data-testid="rating">
      <Rating amount={amount} label={`${amount}`} />
    </Flex>
  );
};

const validCategories = [
  "UNKNOWN",
  "STARS_0",
  "STARS_0_5",
  "STARS_1",
  "STARS_1_5",
  "STARS_2",
  "STARS_2_5",
  "STARS_3",
  "STARS_3_5",
  "STARS_4",
  "STARS_4_5",
  "STARS_5",
  "STARS_6",
  "STARS_7"
];

StarRating.propTypes = {
  /** String __must__ be in the format STARS\_NUM */
  category: PropTypes.oneOf(validCategories)
};

export default StarRating;
