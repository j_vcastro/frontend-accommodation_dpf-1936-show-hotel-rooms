import React from "react";
import { cleanup, render } from "@testing-library/react";
import StarRating from "../index";

afterEach(cleanup);

describe("StarRating", () => {
  test("It should show the correct star rating", async () => {
    const category = "STARS_4";
    const { getByLabelText } = render(<StarRating category={category} />);

    expect(getByLabelText("4")).toBeDefined();
  });

  test("It should not show the star rating if unknown", async () => {
    const category = "UNKNOWN";
    const { queryByTestId } = render(<StarRating category={category} />);

    expect(queryByTestId("rating")).toBe(null);
  });

  test("Check the aria labels", async () => {
    const category = "STARS_4_5";
    const { getByLabelText } = render(<StarRating category={category} />);

    expect(getByLabelText("4.5")).toBeDefined();
  });
});
