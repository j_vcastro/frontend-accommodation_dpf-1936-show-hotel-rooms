import React from "react";
import StarRating from "./index";

export default {
  title: "Components/Star Rating",
  component: StarRating
};

const Template = args => <StarRating {...args} />;

export const Default = Template.bind({});

Default.args = {
  category: "STARS_3"
};
