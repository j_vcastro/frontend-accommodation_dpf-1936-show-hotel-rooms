import React from "react";
import PropTypes from "prop-types";
import { Text } from "prisma-design-system";
import { useTranslation } from "@frontend-shell/react-hooks";

/**
 * The type of Accommodation, renders a __translated text__ based on the incoming type property.<br/>
 */
const AccommodationType = ({ type }) => {
  const { t } = useTranslation();
  const accommodationTypeText = t(`accommodationTypes.${type}`);

  return (
    <Text capitalize fontSize="body.1" themeColor="neutrals.2">
      {accommodationTypeText}
    </Text>
  );
};

const validTypes = [
  "APARTAMENT",
  "BED_AND_BREAKFAST",
  "GUEST_HOUSE",
  "HOSTEL",
  "HOTEL",
  "MOTEL",
  "RESIDENCE",
  "RESORT",
  "RYOKAN",
  "UNKNOWN"
];

AccommodationType.propTypes = {
  type: PropTypes.oneOf(validTypes)
};

export default AccommodationType;
