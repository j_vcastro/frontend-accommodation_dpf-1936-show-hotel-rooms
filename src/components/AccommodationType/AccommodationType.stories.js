import React from "react";
import AccommodationType from "./index";

export default {
  title: "Components/Accommodation Type",
  component: AccommodationType
};

const Template = args => <AccommodationType {...args} />;

export const Default = Template.bind({});

Default.args = {
  type: "HOSTEL"
};
