import React from "react";
import { cleanup } from "@testing-library/react";
import { renderAsync, TestingProviders } from "@testUtils";
import AccommodationType from "../index";

const TRANSLATION = {
  accommodationTypes: {
    HOSTEL: "TYPE_HOSTEL"
  }
};

afterEach(cleanup);

describe("AccommodationType", () => {
  test("It should show the accommodation type", async () => {
    const type = "HOSTEL";
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <AccommodationType type={type} />
      </TestingProviders>
    );

    expect(getByText("TYPE_HOSTEL")).toBeVisible();
  });
});
