import React from "react";
import Tag from "./index";
import { CheckIcon } from "prisma-design-system";

export default {
  title: "Components/Tag",
  component: Tag
};

export const Default = () => {
  return (
    <Tag type="positive" icon={<CheckIcon size="small" />}>
      Default
    </Tag>
  );
};

export const withIcon = () => {
  return [
    { type: "positive", label: "Facility X" },
    { type: "informative", label: "Generic" }
  ].map(({ type, label }) => (
    <Tag type={type} icon={<CheckIcon size="small" />}>
      {label}
    </Tag>
  ));
};

export const withoutIcon = () => {
  return [
    { type: "positive", label: "Free cancellation" },
    { type: "informative", label: "Package Discount" }
  ].map(({ type, label }) => <Tag type={type}>{label}</Tag>);
};
