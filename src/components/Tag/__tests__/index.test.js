import React from "react";
import { cleanup } from "@testing-library/react";
import { axe } from "jest-axe";
import { renderAsync, TestingProviders } from "@testUtils";
import Tag from "../index";
import CheckboxIcon from "prisma-design-system/dist/components/Checkbox/CheckboxIcon";

const TEST = "test";

const TRANSLATION = {
  filters: TEST
};

afterEach(cleanup);

describe("Tag", () => {
  test("It should render their children", async () => {
    const content = "text example";
    const { getByText } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Tag type="positive" icon={<div>icon</div>}>
          {content}
        </Tag>
      </TestingProviders>
    );

    expect(getByText(content)).toBeVisible();
    expect(getByText("icon")).toBeVisible();
  });

  test("should not have basic accessibility issues", async () => {
    const content = "text example";
    const { container } = await renderAsync(
      <TestingProviders translations={TRANSLATION}>
        <Tag type="informative" icon={<CheckboxIcon size="small" />}>
          {content}
        </Tag>
      </TestingProviders>
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
