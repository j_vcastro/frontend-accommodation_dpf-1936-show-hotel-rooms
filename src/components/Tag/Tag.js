import React from "react";
import PropTypes from "prop-types";
import { Flex, Pill, Text, Box } from "prisma-design-system";

const Tag = ({ children, type, icon }) => {
  return (
    <Pill type={type} theme="soft" rounded>
      <Flex flexDirection="row" alignItems="center">
        {icon}
        <Box ml={1} pr={1} pt={0.5}>
          <Text fontSize="body.1">{children}</Text>
        </Box>
      </Flex>
    </Pill>
  );
};

Tag.propTypes = {
  icon: PropTypes.element,
  type: PropTypes.oneOf(["positive", "informative"])
};

Tag.defaultProps = {
  type: "informative"
};

export default Tag;
