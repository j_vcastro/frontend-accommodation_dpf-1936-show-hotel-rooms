import React, { createContext, useContext } from "react";
import { createQueryHook } from "@frontend-shell/react-hooks";

import gql from "graphql-tag";

const MARKETS_NEW_DISPLAY = ["UK", "OPUK"];
const MARKETS_NEW_DISPLAY_TEST = ["FR", "GOFR", "OPFR", "ES", "OPDE"];

export const GET_PRIME_INFO = gql`
  query primeInfoQuery {
    primeInfo {
      isPrimeSite
      isPrimeUser
    }
  }
`;

const useQuery = createQueryHook(GET_PRIME_INFO);

const PrimeQueryContext = createContext();

export const usePrimeQuery = () => useContext(PrimeQueryContext);

const isNewPrime = site => MARKETS_NEW_DISPLAY.includes(site);

const isNewPrimeTest = site => MARKETS_NEW_DISPLAY_TEST.includes(site);

export const usePrime = () => {
  const { loading, error, data } = usePrimeQuery();
  const site = useContext(PrimeSiteContext);
  const isSucceed = !loading && !error;
  const isNewPrimeSite = isSucceed && data.isPrimeSite && isNewPrime(site);
  const isTestNewPrimeSite =
    isSucceed && data.isPrimeSite && isNewPrimeTest(site);
  const isPrimeFooter =
    isSucceed &&
    data.isPrimeSite &&
    !data.isPrimeUser &&
    !isNewPrimeSite &&
    !isTestNewPrimeSite;
  const isPrimeUser = isSucceed && data.isPrimeUser;
  const isPrimeSite = isSucceed && data.isPrimeSite;
  return {
    isNewPrimeSite,
    isTestNewPrimeSite,
    isPrimeFooter,
    isPrimeUser,
    isPrimeSite,
    isDefaultPrimeFooter: isPrimeFooter && site !== "IT",
    isItalyPrimeFooter: isPrimeFooter && site === "IT",
    isPrimeItinerarySidebar: isNewPrimeSite || isTestNewPrimeSite,
    isPrimeFullPriceFooter: isSucceed && data.isPrimeSite && isNewPrimeSite
  };
};

const PrimeQueryProvider = ({ children }) => {
  const primeQueryState = useQuery({});

  const value = {
    ...primeQueryState,
    data: primeQueryState.data?.primeInfo || null
  };

  return (
    <PrimeQueryContext.Provider value={value}>
      {children}
    </PrimeQueryContext.Provider>
  );
};

const PrimeSiteContext = createContext();

const PrimeProvider = ({ site, children }) => (
  <PrimeQueryProvider>
    <PrimeSiteContext.Provider value={site}>
      {children}
    </PrimeSiteContext.Provider>
  </PrimeQueryProvider>
);

export default PrimeProvider;
