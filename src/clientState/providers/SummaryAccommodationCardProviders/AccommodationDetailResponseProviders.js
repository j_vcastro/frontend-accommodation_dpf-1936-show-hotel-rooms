import React, { createContext, useContext } from "react";
import { createQueryHook } from "@frontend-shell/react-hooks";

import { ACCOMMODATION_SUMMARY_QUERY } from "../../../components/SummaryAccommodationCard/query";
import { useAccommodationSummaryRequest } from "../AccommodationSummaryRequestProvider";
import RequestQuery from "./RequestQuery";

const LOADING_STATE = { loading: true, data: null, error: null };

const AccommodationDetailResponseContext = createContext(LOADING_STATE);

const useQuery = createQueryHook(ACCOMMODATION_SUMMARY_QUERY, {
  ensureVisitCookie: false
});

const AccommodationDetailResponseProviders = ({ children }) => {
  const accommodationSummaryRequest = useAccommodationSummaryRequest();
  const accommodationDetailResponse = useQuery(
    RequestQuery(accommodationSummaryRequest)
  );

  return (
    <AccommodationDetailResponseContext.Provider
      value={accommodationDetailResponse}
    >
      {children}
    </AccommodationDetailResponseContext.Provider>
  );
};

const useAccommodationDetailResponse = () =>
  useContext(AccommodationDetailResponseContext);

export default AccommodationDetailResponseProviders;
export { useAccommodationDetailResponse };
