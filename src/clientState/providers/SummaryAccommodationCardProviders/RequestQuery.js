const RequestQuery = accommodationSummaryRequest => ({
  variables: {
    accommodationSummaryRequest: {
      ...accommodationSummaryRequest
    }
  },
  skip: accommodationSummaryRequest === null
});

export default RequestQuery;
