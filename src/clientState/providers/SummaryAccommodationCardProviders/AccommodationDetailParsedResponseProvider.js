import React, { createContext, useContext } from "react";
import { transformSuccessQuery } from "@frontend-shell/react-hooks";
import { useAccommodationDetailResponse } from "./AccommodationDetailResponseProviders";
import { useAccommodationSummaryRequest } from "../AccommodationSummaryRequestProvider";

const AccommodationDetailParsedResponseContext = createContext({});

const transformSearchResponse = queryState =>
  queryState?.data?.accommodationSummary || null;

const checkRoomsHaveSameFacilities = queryState => {
  let roomsHaveSameFacilities = true;
  const {
    roomDeals,
    accommodationDetail
  } = queryState.data.accommodationSummary;
  const baseDeal = roomDeals[0];

  roomDeals.map(elem => {
    elem.paymentAtDestination =
      accommodationDetail.accommodationDealInformation.paymentAtDestination;
    return (roomsHaveSameFacilities =
      elem.boardType === baseDeal.boardType &&
      elem.isCancellationFree === baseDeal.isCancellationFree);
  });

  return roomsHaveSameFacilities;
};

const AccommodationDetailParsedResponseProvider = ({ children }) => {
  const accommodationSummaryRequest = useAccommodationSummaryRequest();
  const queryState = useAccommodationDetailResponse();

  if (queryState.data) {
    queryState.data.accommodationSummary.roomsHaveSameFacilities = checkRoomsHaveSameFacilities(
      queryState
    );
  }

  const nextQueryState = transformSuccessQuery(
    queryState,
    transformSearchResponse,
    accommodationSummaryRequest
  );

  return (
    <AccommodationDetailParsedResponseContext.Provider value={nextQueryState}>
      {children}
    </AccommodationDetailParsedResponseContext.Provider>
  );
};

const useAccommodationDetailParsedResponse = () =>
  useContext(AccommodationDetailParsedResponseContext);
export default AccommodationDetailParsedResponseProvider;
export { useAccommodationDetailParsedResponse };
