import React from "react";

import AccommodationResponseProviders from "./AccommodationDetailResponseProviders";
import AccommodationParsedResponseProvider from "./AccommodationDetailParsedResponseProvider";

import PrimeProvider from "../PrimeProvider";
import AccommodationSummaryRequestProvider from "../AccommodationSummaryRequestProvider";

const SummaryAccommodationCardProviders = ({
  searchId,
  accommodationDealKey,
  roomKeys,
  dedupId,
  site,
  children
}) => (
  <PrimeProvider site={site}>
    <AccommodationSummaryRequestProvider
      searchId={searchId}
      accommodationDealKey={accommodationDealKey}
      roomKeys={roomKeys}
      dedupId={dedupId}
    >
      <AccommodationResponseProviders>
        <AccommodationParsedResponseProvider>
          {children}
        </AccommodationParsedResponseProvider>
      </AccommodationResponseProviders>
    </AccommodationSummaryRequestProvider>
  </PrimeProvider>
);

export default SummaryAccommodationCardProviders;
