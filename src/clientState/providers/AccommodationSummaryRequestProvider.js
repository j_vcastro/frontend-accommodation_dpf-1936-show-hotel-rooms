import React, { createContext, useContext } from "react";

const AccommodationSummaryRequestContext = createContext(null);

const AccommodationSummaryRequestProvider = ({
  searchId,
  accommodationDealKey,
  roomKeys,
  dedupId,
  children
}) => {
  const accommodationSummaryRequest = {
    searchId: parseInt(searchId),
    dedupId: parseInt(dedupId),
    accommodationDealKey,
    roomKeys
  };
  return (
    <AccommodationSummaryRequestContext.Provider
      value={accommodationSummaryRequest}
    >
      {children}
    </AccommodationSummaryRequestContext.Provider>
  );
};

const useAccommodationSummaryRequest = () =>
  useContext(AccommodationSummaryRequestContext);

export default AccommodationSummaryRequestProvider;
export { useAccommodationSummaryRequest };
