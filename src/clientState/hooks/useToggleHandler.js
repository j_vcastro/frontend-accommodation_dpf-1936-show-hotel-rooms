import { useCallback, useState } from "react";

/**
 * Generic handler to toggle a boolean state
 * @param {boolean} open - initial state
 * @param {Function} [onToggle= () => {}] - your callback
 * @returns {[boolean, Function]} the function is a useCallback type
 */
const useToggleHandler = (open, onToggle = () => {}) => {
  const [isOpen, setOpen] = useState(open);

  const handleToogle = useCallback(() => {
    setOpen(!isOpen);
    onToggle(!isOpen);
  }, [isOpen, onToggle]);
  return [isOpen, handleToogle];
};

export default useToggleHandler;
