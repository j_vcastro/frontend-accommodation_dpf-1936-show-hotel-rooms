import { createTrackingTestExtensions } from "@frontend-shell/react-hooks";

expect.extend({
  ...createTrackingTestExtensions({
    category: "flights_results",
    aliases: {
      toTrackNthWith: "toTrackResultsNthWith",
      toTrackOnceWith: "toTrackResultsOnceWith"
    }
  }),
  ...createTrackingTestExtensions({
    category: "flights_summary",
    aliases: {
      toTrackNthWith: "toTrackTripSummaryNthWith",
      toTrackOnceWith: "toTrackTripSummaryOnceWith"
    }
  })
});
