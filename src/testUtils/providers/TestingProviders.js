import React from "react";
import enLocale from "date-fns/locale/en-GB";

import { createTranslationsModuleProvider } from "@frontend-shell/react-hooks";

import AccommodationDetailProviders from "../../clientState/providers/SummaryAccommodationCardProviders";
import MockedAppProviders from "./MockedAppProviders";
import { AccommodationSummaryRequest } from "../../serverMocks";

const MODULE_NAMESPACE = "accommodation";

export default function TestingProviders({
  translations,
  i18nextOptions,
  site = "PT",
  locale = "en",
  themeName,
  response,
  loading,
  trackPage = () => {},
  window,
  Odigeo,
  brand,
  currencyLocale,
  dateFnsLocale = enLocale,
  abs,
  request,
  children
}) {
  const TranslationsAccommodationProvider = createTranslationsModuleProvider({
    locale,
    namespace: MODULE_NAMESPACE,
    translations
  });
  const {
    searchId,
    accommodationDealKey,
    roomKeys,
    dedupId
  } = AccommodationSummaryRequest(request);

  return (
    <MockedAppProviders
      i18nextOptions={i18nextOptions}
      site={site}
      locale={locale}
      brand={brand}
      themeName={themeName}
      currencyLocale={currencyLocale}
      dateFnsLocale={dateFnsLocale}
      mocks={response}
      Odigeo={Odigeo}
      loading={loading}
      trackPage={trackPage}
      window={window}
      abs={abs}
    >
      <TranslationsAccommodationProvider>
        <AccommodationDetailProviders
          searchId={searchId}
          accommodationDealKey={accommodationDealKey}
          roomKeys={roomKeys}
          site={site}
          dedupId={dedupId}
        >
          {children}
        </AccommodationDetailProviders>
      </TranslationsAccommodationProvider>
    </MockedAppProviders>
  );
}
