import React from "react";
import { PrismaProvider, useMediaQueries, themes } from "prisma-design-system";
import {
  TrackingProvider,
  TrackingMetricsProvider,
  IntegrationProvider,
  BrandProvider,
  SiteProvider,
  TranslationsProvider
} from "@frontend-shell/react-hooks";
import MockedQueriesProvider from "./MockedQueriesProvider";

const defaultCurrencyLocale = { locale: "en-GB" };
const noop = Function.prototype;

const mockOdigeoForTest = {
  trigger: noop,
  Session: {
    set: noop
  }
};

const AppTrackingProvider = ({ children, trackPage, window }) => {
  const { md } = useMediaQueries();
  return (
    <TrackingProvider
      flow={md ? "D" : "M"}
      window={window}
      trackPage={trackPage}
    >
      {children}
    </TrackingProvider>
  );
};

export default function MockedAppProviders({
  i18nextOptions,
  mocks,
  loading,
  trackPage,
  window: windowMock,
  brand = "ED",
  site,
  themeName,
  Odigeo = mockOdigeoForTest,
  currencyLocale = defaultCurrencyLocale,
  dateFnsLocale,
  abs = [],
  children
}) {
  const theme = themes[themeName] || themes.ED;
  Object.assign(window, windowMock);
  return (
    <TranslationsProvider options={i18nextOptions}>
      <PrismaProvider
        theme={theme}
        currencyLocale={currencyLocale}
        dateFnsLocale={dateFnsLocale}
      >
        <SiteProvider site={site}>
          <BrandProvider brand={brand}>
            <AppTrackingProvider trackPage={trackPage} window={window}>
              <TrackingMetricsProvider>
                <IntegrationProvider Odigeo={Odigeo}>
                  <MockedQueriesProvider
                    mocks={mocks}
                    loading={loading}
                    abs={abs}
                  >
                    {children}
                  </MockedQueriesProvider>
                </IntegrationProvider>
              </TrackingMetricsProvider>
            </AppTrackingProvider>
          </BrandProvider>
        </SiteProvider>
      </PrismaProvider>
    </TranslationsProvider>
  );
}
