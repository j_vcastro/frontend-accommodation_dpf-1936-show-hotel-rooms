import React from "react";
import {
  getVariationsResources,
  VersionedComponentsProvider
} from "@frontend-shell/react-hooks";
import ResultsVersionedComponents from "../../versionedComponents";
import AutoMockedProvider from "../AutoMockedProvider";

export default function MockedQueriesProvider({
  mocks,
  loading,
  abs,
  children
}) {
  const { activeAliases, variations } = getVariationsResources(abs);
  return (
    <AutoMockedProvider
      mocks={{
        Query: () => ({ variations }),
        ...mocks
      }}
      loading={loading}
    >
      <VersionedComponentsProvider
        resources={ResultsVersionedComponents}
        activeAliases={activeAliases}
      >
        {children}
      </VersionedComponentsProvider>
    </AutoMockedProvider>
  );
}
