import { wait } from "@testing-library/react";

/**
 * @description It returns a function that wait until the callback does not throw an error.-
 *
 * I created this wrapper function because the wait function in testing-library/react is DEPRECATED.
 * The wait function will be replaced for waitFor function.
 *
 * IMPORTANT:
 * The signature is the same both BUT...
 * wait function: The callback param is optional.-
 * waitFor: The callback IS REQUIRED.-
 *
 * @example await waitAsync(() => getByText('Text we are waiting for'));
 *
 * @param {Function} [callback] The expression that will be evaluated until it doesn't throw an error
 * @param {Object} [options]
 * @param {HTMLElement} [options.container]
 * @param {number} [options.timeout]
 * @param {number} [options.interval]
 * @param {MutationObserverInit} [options.mutationObserverOptions]
 *
 * @returns async {Function}
 */
const waitAsync = (callback, options) => wait(callback, options);

export default waitAsync;
