import { act, fireEvent } from "@testing-library/react";

const asyncFireEvent = {
  async change(...args) {
    return await act(async () => {
      fireEvent.change(...args);
    });
  },
  async click(...args) {
    return await act(async () => {
      fireEvent.click(...args);
    });
  }
};

export default asyncFireEvent;
