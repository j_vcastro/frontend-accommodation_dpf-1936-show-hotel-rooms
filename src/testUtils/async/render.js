import { render } from "@testing-library/react";
import waitAsync from "./waitAsync";

const renderAsync = async (element, options) => {
  const result = render(element, options);

  await waitAsync();
  return result;
};

export default renderAsync;
