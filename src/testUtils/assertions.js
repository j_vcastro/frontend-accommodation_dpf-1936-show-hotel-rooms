import { getByText, getAllByText, getAllByTestId } from "@testing-library/dom";

const getByTextUsingRegExp = (container, expected) => {
  try {
    getAllByText(container, (content, node) => expected.test(node.textContent));
  } catch (error) {
    error.message = error.message.replace(/function[^}]*\}/, expected);
    throw error;
  }
};

const assertions = ({ container }) => {
  return {
    itineraries: () => ({
      haveTextsInOrder(texts) {
        const itineraries = getAllByTestId(container, "itinerary");

        expect(itineraries.length).toBe(texts.length);
        itineraries.forEach((itinerary, index) => {
          const text = texts[index];
          const getByTextFunction =
            typeof text === "string" ? getByText : getByTextUsingRegExp;
          getByTextFunction(itinerary, text);
        });
      }
    })
  };
};

export const assertMetric = () => {
  const bodyCalls = fetch.mock.calls.map(
    call => call[1].body && JSON.parse(call[1].body)
  );
  const filterMetricsWithKey = (bodyCalls, expectedBody) =>
    bodyCalls.filter(body => body && expectedBody.key === body.key);

  return {
    toHaveBeenCalledOnceWith(expectedBody) {
      expect(filterMetricsWithKey(bodyCalls, expectedBody)).toHaveLength(1);
    },
    toNotHaveBeenCalledWith(expectedBody) {
      expect(filterMetricsWithKey(bodyCalls, expectedBody)).toHaveLength(0);
    }
  };
};

export default assertions;
