import matchMediaPolyfill from "mq-polyfill";
const { defineProperty } = Object;

matchMediaPolyfill(global.window);

// https://spectrum.chat/testing-library/help-react/how-to-set-window-innerwidth-to-test-mobile~70aa9572-b7cc-4397-92f5-a09d75ed24b8
export const setViewport = ({ width, height }) => {
  defineProperty(global.window, "innerWidth", {
    writable: true,
    configurable: true,
    value: width || global.window.innerWidth
  });
  defineProperty(global.window, "innerHeight", {
    writable: true,
    configurable: true,
    value: height || global.window.innerHeight
  });
  global.window.dispatchEvent(new window.Event("resize"));
};
