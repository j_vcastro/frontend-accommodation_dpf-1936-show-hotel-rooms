const MODAL_TRANSITION = 200;

const DRAWER_TRANSITION = 500;

export const waitForModalAppearance = () =>
  jest.advanceTimersByTime(MODAL_TRANSITION);
export const waitForModalDisappearance = () =>
  jest.advanceTimersByTime(MODAL_TRANSITION);

export const waitForDrawerAppearance = () =>
  jest.advanceTimersByTime(DRAWER_TRANSITION);
export const waitForDrawerDisappearance = () =>
  jest.advanceTimersByTime(DRAWER_TRANSITION);
