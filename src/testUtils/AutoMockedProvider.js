import React from "react";
import {
  AutoMockedClient,
  GraphQLClientProvider
} from "@frontend-shell/react-hooks";
import introspectionResult from "../../schema.json";

const schema = introspectionResult.__schema;

const AutoMockedProvider = ({ mocks, loading, children }) => {
  const client = AutoMockedClient({ schema, mocks, loading });
  return (
    <GraphQLClientProvider client={client}>{children}</GraphQLClientProvider>
  );
};

export default AutoMockedProvider;
