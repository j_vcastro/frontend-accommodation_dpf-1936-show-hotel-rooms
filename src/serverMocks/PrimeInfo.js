/** @typedef {Object} PrimeInfo
 * @property {Boolean} isPrimeSite
 * @property {Boolean} isPrimeUser
 */
/**
 * PrimeInfo factory
 * @param {Boolean} [isPrimeUser=false]
 * @param {Boolean} [isPrimeSite=false]
 * @returns {Function}
 * @constructor
 */
const PrimeInfo = ({ isPrimeUser = false, isPrimeSite = false }) => {
  /**
   * @returns {PrimeInfo}
   */
  return () => ({
    isPrimeUser,
    isPrimeSite
  });
};

export default PrimeInfo;
