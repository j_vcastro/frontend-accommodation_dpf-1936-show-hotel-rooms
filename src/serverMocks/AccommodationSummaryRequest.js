const defaultAccommodationSummaryRequest = {
  searchId: 99999,
  accommodationDealKey: "612",
  roomKeys: ["0"],
  dedupId: 53213
};

const AccommodationSummaryRequest = (accommodationSummaryRequest = {}) => ({
  ...defaultAccommodationSummaryRequest,
  ...accommodationSummaryRequest
});

export default AccommodationSummaryRequest;
